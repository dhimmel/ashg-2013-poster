# open from heteronets

import nxutils
import metapaths
import schema 

pkl_path = '/home/dhimmels/Documents/serg/ashg13/130523-2/prepared-graph.pkl'
g = nxutils.read_gpickle(pkl_path)


ppi_association_mpath = schema.MetaPath(('disease', 'association', 'gene', 'interaction', 'gene', 'association', 'disease'))
paths = metapaths.paths_between(g, 'EFO_0000676', 'EFO_0003885', ppi_association_mpath)
for path in paths:
    print path

tissue_mpath = schema.MetaPath(('gene', 'specificity', 'tissue', 'specificity', 'gene'))
paths = metapaths.paths_between(g, 'EFO_0000676', 'EFO_0003885', ppi_association_mpath)
for path in paths:
    print path


# Multiple Sclerosis EFO_0003885
# Type II Diabetes EFO_0001360

# Alzheimer's EFO_0000249
# Psoriasis EFO_0000676

# MS and Diabetes shared neighbors
# HHEX and THADA
set(g.neighbors('EFO_0003885')) & set(g.neighbors('EFO_0001360'))

set(g.neighbors('EFO_0000676'))

#g.graph['schema']

tissue_mpath = schema.MetaPath(('gene', 'specificity', 'tissue', 'pathology', 'disease'))
ppi_mpath = schema.MetaPath(('gene', 'interaction', 'gene', 'association', 'disease'))
gwas_mpath = schema.MetaPath(('gene', 'association', 'disease', 'association', 'gene', 'association', 'disease'))

metapaths.paths_between(g, 'MOG', 'EFO_0003885', tissue_mpath)
g.neighbors('EFO_0003885')

metapaths.paths_between(g, 'MOG', 'EFO_0003885', ppi_mpath)
metapaths.paths_between(g, 'MOG', 'EFO_0003885', gwas_mpath)

['MOG', 'association', 'EFO_0000384', 'association', 'TYK2', 'association', 'EFO_0003885']
