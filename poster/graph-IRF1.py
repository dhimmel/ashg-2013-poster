import pprint

import hetnet
import hetnet.readwrite.graph
import hetnet.algorithms

pkl_path = '/home/dhimmels/Documents/serg/ashg13/131014-1/graph/graph.pkl.gz'
graph = hetnet.readwrite.graph.read_pickle(pkl_path)

JAK2 = graph.node_dict['JAK2']
IRF1 = graph.node_dict['IRF1']
REL = graph.node_dict['REL']
IKZF3 = graph.node_dict['IKZF3']
CD48 = graph.node_dict['CD48']
ms = graph.node_dict['DOID:2377']

metapaths = graph.metagraph.extract_metapaths('gene', 'disease', 4)
metapath_subset = [metapaths[1], metapaths[2], metapaths[3], metapaths[4], metapaths[5], metapaths[6]]

for metapath in metapath_subset:
	print metapath
	paths = graph.paths_between(IRF1, ms, metapath)
	pprint.pprint(paths)




