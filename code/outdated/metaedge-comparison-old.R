source('feature-parser.R')



learning.features.path <- '/home/dhimmels/Documents/serg/ashg13/131010-1/GaD-both/analyses/learning-edges-dwpc0.5/learning-features.txt'
prob.positive <- 0.01
percent.testing <- 1 / 3
learning.tab <- ReadFeatures(learning.features.path)
learning.tab <- AddWeight(learning.tab, prob.positive)
terms <- TermSubset(learning.tab, prefixes=c('DWPC_0.5'))
metapaths <- sapply(strsplit(terms, '_', fixed = TRUE), function(x) x[length(x)])

# Filter diseases
#genes.per.disease <- table(subset(learning.tab, status == 1)$target)
#keep.diseases <- names(genes.per.disease[genes.per.disease >= 20])
#learning.tab <- subset(learning.tab, target %in% keep.diseases)


train.test.list <- SplitTesting(learning.tab, learning.tab[, 'status'])
training.tab <- train.test.list$training
testing.tab <- train.test.list$testing


training.y <- training.tab[, 'status']
training.w <- training.tab[, 'weight']
testing.y <- testing.tab[, 'status']




library(ROCR)
metaedge.df <- data.frame()
metaedge.exclusions <- list('None', 'GiG', 'GfG', 'DsD', 'GeT', 'DcT')
for (metaedge.exclusion in metaedge.exclusions) {
  metapath.subset <- MetaPathSubset(metapaths, metaedges=c(metaedge.exclusion), exclude=TRUE)
  suffixes <- paste('_', metapath.subset, sep='')
  term.subset <- TermSubset(learning.tab, prefixes=c('DWPC_0.5'), suffixes=suffixes)
  training.x <- as.matrix(training.tab[, term.subset])
  testing.x <- as.matrix(testing.tab[, term.subset])
  glm.alpha <- 0
  enet.training <- glmnet::cv.glmnet(training.x, training.y, training.w, alpha=glm.alpha, family='binomial')
  #coef(enet.training, s=enet.training$lambda.1se)
  enet.test.preds <- predict(enet.training, newx=testing.x, type='response')
  rocr.pred <- ROCR::prediction(enet.test.preds, testing.y)
  auc <- ROCR::performance(rocr.pred, 'auc')@y.values[[1]]
  i <- nrow(metaedge.df) + 1
  metaedge.df[i, 'metaedge'] <- metaedge.exclusion
  metaedge.df[i, 'kind'] <- 'exclusion'
  metaedge.df[i, 'auc'] <- auc
  #rocr.list[[metaedge.exclusion]] <- rocr.pred
}

metaedge.exclusions <- list('GiG', 'GfG', 'DsD', 'GeT', 'DcT')
for (metaedge.exclusion in metaedge.exclusions) {
  metapath.subset <- MetaPathSubset(metapaths, metaedges=c('DaG', metaedge.exclusion), exclude=FALSE)
  suffixes <- paste('_', metapath.subset, sep='')
  term.subset <- TermSubset(learning.tab, prefixes=c('DWPC_0.5'), suffixes=suffixes)
  training.x <- as.matrix(training.tab[, term.subset])
  testing.x <- as.matrix(testing.tab[, term.subset])
  glm.alpha <- 0
  enet.training <- glmnet::cv.glmnet(training.x, training.y, training.w, alpha=glm.alpha, family='binomial')
  #coef(enet.training, s=enet.training$lambda.1se)
  enet.test.preds <- predict(enet.training, newx=testing.x, type='response')
  rocr.pred <- ROCR::prediction(enet.test.preds, testing.y)
  auc <- ROCR::performance(rocr.pred, 'auc')@y.values[[1]]
  i <- nrow(metaedge.df) + 1
  metaedge.df[i, 'metaedge'] <- metaedge.exclusion
  metaedge.df[i, 'kind'] <- 'inclusion'
  metaedge.df[i, 'auc'] <- auc
}

library(ggplot2)
ggplot(metaedge.df, aes(metaedge, auc, color=kind)) + geom_point()


