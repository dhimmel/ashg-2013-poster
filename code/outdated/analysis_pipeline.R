options(stringsAsFactors = FALSE)
#options(width=Sys.getenv("COLUMNS"))
p <- function(...) paste(..., sep='')

network.id <- '130717-1'
ashg.dir <- '/home/dhimmels/Documents/serg/ashg13/'
network.dir <- file.path(ashg.dir, network.id)
feature.dir <- file.path(network.dir, 'features')
graphics.dir <- file.path(network.dir, 'graphics')
integrations.dir <- file.path(network.dir, 'integrations')

CalculatePPA <- function(prior, pval) {
  # http://www.nature.com/nrg/journal/v10/n10/full/nrg2615.html
  # BF - Bayes Factor
  # PO - Posterior Odds
  # PPA - Posterior Probability of Association
  BF <- -1 / (exp(1) * pval * log(pval))
  BF[pval > exp(-1)] <- NA
  PO <- BF * prior / (1 - prior)
  PPA <- PO / (1 + PO)
  return(PPA)
}

GetPPV <- function(scores, actual, threshold, neg.weight=1, pos.weight=1) {
  # Calculate the Positive Predictive Value (precision).
  complete.indices <- ! (is.na(scores) | is.na(actual))
  scores <- scores[complete.indices]
  actual <- actual[complete.indices]
  classification <- scores > threshold
  number.TP <- sum(classification & actual)
  number.FP <- sum(classification & ! actual)
  PPV <- number.TP * pos.weight / (number.TP * pos.weight + number.FP * neg.weight)
  return(PPV)
}

PvalBlocks <- function(pvals) {
  # LD blocks inferred from p-values below 0.05
  blocks <- 1 + cumsum(pvals > 0.05)
  blocks[pvals > 0.05] <- NA
  return(blocks)
}

library(ROCR)
FitAndPlotROCs <- function(tab) {
  #

  formulas.list <- list()
  ll <- list()
  ll[['name']] <- 'Combined'
  ll[['col']] <- 'black'
  ll[['formula']] <- as.formula('status ~ 1 + NPC_GiGaD + NPC_GsTpD + NPC_GaDaGaD')
  formulas.list[[ll[['name']]]] <- ll

  ll <- list()
  ll[['name']] <- 'Complete'
  ll[['col']] <- 'darkgrey'
  ll[['formula']] <- as.formula('status ~ 1 + NPC_GaD + NPC_GiGaD + NPC_GsTpD + NPC_GiGiGaD + NPC_GiGsTpD + NPC_GsTsGaD + NPC_GaDaGaD + NPC_GaDpTpD')
  formulas.list[[ll[['name']]]] <- ll

  #ll <- list()
  #ll[['name']] <- 'Combined Mixture Model'
  #ll[['col']] <- 'grey'
  #ll[['formula']] <- as.formula('status ~ 1 + (NPC_GiGaD == 0) + NPC_GiGaD + (NPC_GsTpD == 0) + NPC_GsTpD + (NPC_GaDaGaD == 0) + NPC_GaDaGaD')
  #formulas.list[[ll[['name']]]] <- ll

  ll <- list()
  ll[['name']] <- 'PPI'
  ll[['col']] <- 'blue'
  ll[['formula']] <- as.formula('status ~ 1 + NPC_GiGaD')
  formulas.list[[ll[['name']]]] <- ll

  #ll <- list()
  #ll[['name']] <- 'PPI Mixture Model'
  #ll[['col']] <- 'lightblue'
  #ll[['formula']] <- as.formula('status ~ 1 + (NPC_GiGaD == 0) + NPC_GiGaD')
  #formulas.list[[ll[['name']]]] <- ll

  ll <- list()
  ll[['name']] <- 'Tissue'
  ll[['col']] <- 'darkgreen'
  ll[['formula']] <- as.formula('status ~ 1 + NPC_GsTpD')
  formulas.list[[ll[['name']]]] <- ll

  ll <- list()
  ll[['name']] <- 'Diseasome'
  ll[['col']] <- 'red'
  ll[['formula']] <- as.formula('status ~ 1 + NPC_GaDaGaD')
  formulas.list[[ll[['name']]]] <- ll


par(mar=c(2.1, 2.1, 1.5, 0.5), # margins bottom, left, top, right
  mgp=c(1.1, .25, 0), # axis title, labels, and ticks
  tcl=-0.25, # tick length
  lwd= 1.8) # line width

plot(0, 0, type='n', xlim=c(0, 1), ylim=c(0, 1), xlab='False positive rate', ylab='True positive rate',
  main='Global ROC Curves')
lines(0:1, 0:1, lty=2)
legend.cols <- rep(NA, length(formulas.list))
legend.labels <- rep(NA, length(formulas.list))
for (i in 1:length(formulas.list)) {
  ll <- formulas.list[[i]]
  model <- glm(ll[['formula']], family='binomial', data=tab, weights=weight, na.action='na.exclude')
  scores <- predict(model, type='response')
  rocr.prediction <- prediction(scores, tab$status)
  roc <- performance(rocr.prediction, 'tpr', 'fpr')
  auc <- performance(rocr.prediction, 'auc')@y.values[[1]]
  #auc <- getPPV(scores, tab$status)
  #auc <- performance(rocr.prediction, 'auc', fpr.stop=0.15)@y.values[[1]]
  ppv <- GetPPV(scores, tab$status, prob.positive, neg.weight, pos.weight)
  lines(roc@x.values[[1]], roc@y.values[[1]], col=ll[['col']])
  legend.cols[[i]] <- ll[['col']]
  legend.labels[[i]] <- p(ll[['name']], ': ', round(auc, 3), ': ', round(ppv, 4))
}
legend('bottomright', lty=1, col=legend.cols, title='model: AUC: PPV', cex=0.85,
  y.intersp=.9, legend=legend.labels, bty='n')
}



################################################################################
###################### Create the learned model ################################
#DAAG::cv.binary(glm, print.detail=True)
pdf(file.path(graphics.dir, 'global-ROCs-NPC.pdf'), width=6, height=6)
cat('Model Learning Stage\n')
prob.positive <- 0.01

learning.features.path <- file.path(feature.dir, 'learning-features-damp-0.5.txt')
tab <- read.delim(learning.features.path)

# Create positive and negative weights
tab$weight <- NA
neg.weight <- (1 - prob.positive) * sum(tab$status == 1) / (prob.positive * sum(tab$status == 0))
pos.weight <- 1
tab$weight[tab$status == 0] <- neg.weight
tab$weight[tab$status == 1] <- pos.weight
cat(p('Negative Weight: ', round(neg.weight, 4), ' Positive Weight: ', pos.weight, '\n'))

# Create model
#glm.terms <- npc_terms[npc_terms != 'NPC_GaD']
glm.terms <- colnames(tab)[substr(colnames(tab), 1, 3) == 'NPC']
glm.terms <- glm.terms[glm.terms != 'NPC_GaD']
glm.terms <- c('NPC_GiGaD', 'NPC_GsTpD', 'NPC_GaDaGaD')
logreg_formula <- as.formula(paste('status ~ 1 + ', paste(glm.terms, collapse=' + '), sep=''))
logreg <- glm(logreg_formula, family='binomial', data=tab, weights=weight, na.action='na.exclude')
tab$prediction <- predict(logreg, type='response')
ppv <- GetPPV(tab$prediction, tab$status, threshold=prob.positive, neg.weight, pos.weight)
cat(p('Training Precision: ', round(ppv, 4), '\n'))

FitAndPlotROCs(tab)
dev.off()


logreg_formula <- as.formula('status ~ 1 + (NPC_GiGaD == 0) + log(NPC_GiGaD + 1)')
logreg_formula <- as.formula('status ~ 1 + (NPC_GiGaD == 0) + NPC_GiGaD + (NPC_GsTpD == 0) + (NPC_GaDaGaD == 0) + NPC_GaDaGaD')
logreg_formula <- as.formula('status ~ 1 + NPC_GiGaD')
logreg <- glm(logreg_formula, family='binomial', data=tab, weights=weight, na.action='na.exclude')
summary(logreg)
################################################################################
###################### Predict Diseases of Interest ############################
pdf(file.path(graphics.dir, 'disease-specific-ROCs-mixture-model.pdf'), width =6, height=6)
disease.feature.dir <- file.path(feature.dir, 'disease')
diseases <- sapply(strsplit(list.files(disease.feature.dir), "\\."), function (x) x[1])
summary.tab <- data.frame()
for (disease in diseases) {

  feature.path <- file.path(disease.feature.dir, p(disease, '.txt'))
  feature.tab <- read.delim(feature.path)
  feature.tab$status <- as.integer(feature.tab$status == 1)
  feature.tab$prediction <- predict(logreg, type='response', newdata=feature.tab)
  if (sum(feature.tab$status[! is.na(feature.tab$prediction)]) == 0)
    next
  
  # plot ROC
  par(mar=c(2.1, 2.1, 1.5, 0.5), # margins bottom, left, top, right
    mgp=c(1.1, .25, 0), # axis title, labels, and ticks
    tcl=-0.25, # tick length
    lwd= 1.8) # line width
  #par(mfrow=c(2,2))
  pred <- prediction(feature.tab$prediction, feature.tab$status)
  perf <- performance(pred, 'tpr', 'fpr')
  auc <- performance(pred, 'auc')@y.values[[1]]
  ppv <- GetPPV(feature.tab$prediction, feature.tab$status, prob.positive)
  npos <- sum(feature.tab$status)
  plot(perf, main=disease)
  lines(0:1, 0:1, lty=2)
  text(0.8, 0.2, labels=p('AUC: ', round(auc, 3), '\nPPV: ', round(ppv, 5), '\nPositives: ', npos))
  summary.tab <- rbind(summary.tab, c(auc, ppv, npos))
  rownames(summary.tab)[nrow(summary.tab)] <- disease
}

colnames(summary.tab) <- c('auc', 'ppv', 'npos')
library(gplots)
gplots::textplot(round(summary.tab, 4))

summary.tab <- summary.tab[order(summary.tab$auc, decreasing=TRUE), ]
gplots::textplot(round(summary.tab, 4))
dev.off()
summary.tab[summary.tab$npos > 50,]




################################################################################
###################### Predict Diseases of Interest ############################
cat('Prediction Stage\n')
gwas.dir <- '/home/dhimmels/Documents/serg/data-sources/dbgap/vegas/'

TopInBlockTab <- function(tab, block.column) {
  tab <- tab[! duplicated(tab[, block.column]) & ! is.na(tab[, block.column]), ]
  return(tab)
}
gene.wise.gwas.list <- list()
gene.wise.gwas.list[['multiple sclerosis']] <- c('phs000171.pha002861', 'phs000139.pha002854')
gene.wise.gwas.list[['systemic lupus erythematosus']] <- c('phs000122.pha002848', 'phs000216.pha002867')
gene.wise.gwas.list[['type I diabetes mellitus']] <- c('phs000018.pha002864', 'phs000180.pha002862')
gene.wise.gwas.list[['amyotrophic lateral sclerosis']] <- c('phs000101.pha002846', 'phs000127.pha002851')
gene.wise.gwas.list[['age-related macular degeneration']] <- c('phs000001.pha002856', 'phs000182.pha002890')

pdf(file.path(graphics.dir, 'gwas-integrations.pdf'), width =6, height=6)
diseases <- names(gene.wise.gwas.list)
for(disease in diseases) {
  cat(p('------------------------', disease, '-------------------\n'))
  feature.path <- file.path(feature.dir, 'disease', p(disease, '.txt'))
  feature.tab <- read.delim(feature.path)
  feature.tab$status <- as.integer(feature.tab$status == 1)
  feature.tab$prediction <- predict(logreg, type='response', newdata=feature.tab)
  disease.tab <- data.frame('gene'=feature.tab$source, 'disease'=feature.tab$target,
    'status'=feature.tab$status, 'prediction'=feature.tab$prediction)

  gwases <- gene.wise.gwas.list[[disease]]  
  par(mfrow = c(1, length(gwases)))
  for (gwas.id in gwases) {
    gwas.path <- file.path(gwas.dir, p(gwas.id, '-hapmapCEU.out'))
    gwas <- read.table(gwas.path, header=TRUE)
    gwas <- gwas[order(gwas$Chr, (gwas$Start + gwas$Stop) / 2), ] # sort by location
    gwas$block <- PvalBlocks(gwas$Pvalue)
    gwas.matches <- match(disease.tab$gene, gwas$Gene)
    disease.tab[, p('P.', gwas.id)] <- gwas[gwas.matches, 'Pvalue']
    disease.tab[, p('block.', gwas.id)] <- gwas[gwas.matches, 'block']
    disease.tab[, p('PPA.', gwas.id)] <- CalculatePPA(disease.tab$prediction, disease.tab[, p('P.', gwas.id)])
    hist(gwas$Pvalue, breaks=40, xlab=p(disease, ' P value'), freq=FALSE, main=gwas.id)
    abline(h=1, col='red', lty=2, lwd=2)
  }
  par(mfrow = c(1, 1))
  disease.tab <- cbind(disease.tab, feature.tab[, glm.terms])
  disease.tab <- disease.tab[order(disease.tab$prediction, decreasing=TRUE), ]

  known.genes <- disease.tab[as.logical(disease.tab$status), 'gene']
  known.ld.genes <- known.genes

  for (gwas.id in gwases) {
    known.blocks <- disease.tab[as.logical(disease.tab$status), p('block.', gwas.id)]
    known.blocks <- na.omit(known.blocks)
    known.blocks <- unique(known.blocks)
    genes <- disease.tab[disease.tab[, p('block.', gwas.id)] %in% known.blocks, 'gene']
    known.ld.genes <- c(known.ld.genes, genes)
  }

  known.ld.genes <- unique(known.ld.genes)
  disease.tab$ld.with.positive <- as.integer(disease.tab$gene %in% known.ld.genes)
  novel.tab <- disease.tab[! disease.tab$ld.with.positive, ]
  novel.tab <- novel.tab[! apply(is.na(novel.tab[, p('P.', gwases)]), 1, any), ]
  #head(novel.tab[novel.tab[, p('P.', gwases[1])] <= 0.05, ], )
  
  disease.dir <- file.path(integrations.dir, disease)
  if (! file.exists(disease.dir)) dir.create(disease.dir)

  path <- file.path(disease.dir, 'all-genes.txt')
  write.table(disease.tab, path, sep='\t', quote=FALSE, row.names=FALSE, na='')

  path <- file.path(disease.dir, 'novel-genes.txt')
  write.table(novel.tab, path, sep='\t', quote=FALSE, row.names=FALSE, na='')

  for (gwas.id in gwases) {
    marginal.tab <- disease.tab[disease.tab[, p('P.', gwas.id)] <= 0.05, ]
    #marginal.tab <- marginal.tab[order(marginal.tab[, p('PPA.', gwas.id)], decreasing=TRUE), ]
    path <- file.path(disease.dir, p('all-genes-topinblock-marginal-in-', gwas.id, '.txt'))
    write.table(TopInBlockTab(marginal.tab, p('block.', gwas.id)), path, sep='\t', quote=FALSE, row.names=FALSE, na='')

    marginal.novel.tab <- novel.tab[novel.tab[, p('P.', gwas.id)] <= 0.05, ]
    #marginal.novel.tab <- marginal.novel.tab[order(marginal.novel.tab[, p('PPA.', gwas.id)], decreasing=TRUE), ]
    path <- file.path(disease.dir, p('novel-genes-topinblock-marginal-in-', gwas.id, '.txt'))
    write.table(TopInBlockTab(marginal.novel.tab, p('block.', gwas.id)), path, sep='\t', quote=FALSE, row.names=FALSE, na='')
  }
  

  novel.tab <- novel.tab[order(novel.tab$prediction, decreasing=TRUE), ]
  marginal.all.gwas <- rowSums(novel.tab[, p('P.', gwases)] > 0.05) == 0
  marginal.any.gwas <- rowSums(novel.tab[, p('P.', gwases)] <= 0.05) > 0
  cat('Probability random gene will be marginally significant accross any GWAS:\n')
  cat(mean(marginal.any.gwas)); cat('\n')
  print(table(marginal.any.gwas))
  cat('Probability positively classified novel gene is marginally significant accross any GWAS:\n')
  putative.gene.number <- round(nrow(novel.tab) * prob.positive)
  cat(mean(marginal.any.gwas[1:putative.gene.number])); cat('\n')
  print(table(marginal.any.gwas[1:putative.gene.number]))
  

  # plot ROC
  par(mar=c(2.1, 2.1, 1.5, 0.5), # margins bottom, left, top, right
    mgp=c(1.1, .25, 0), # axis title, labels, and ticks
    tcl=-0.25, # tick length
    lwd= 1.8) # line width
  #par(mfrow=c(2,2))
  pred <- prediction(disease.tab$prediction, disease.tab$status)
  perf <- performance(pred, 'tpr', 'fpr')
  auc <- performance(pred, 'auc')@y.values[[1]]
  ppv <- GetPPV(disease.tab$prediction, disease.tab$status, prob.positive)
  plot(perf, main=disease)
  lines(0:1, 0:1, lty=2)
  text(0.8, 0.2, labels=p('AUC: ', round(auc, 3), '\nPPV: ', round(ppv, 5)))
}

dev.off()























