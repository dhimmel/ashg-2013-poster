library('ROCR')


options(stringsAsFactors = FALSE)
network.id = '130717-1'
path = file.path('/home/dhimmels/Documents/serg/ashg13/', network.id, 'features', 'learning-features-damp-1.0.txt')
tab <- read.delim(path)
remove.col <- apply(tab, 2, function(col) all(is.na(col)))
tab <- tab[, ! remove.col]

npc_terms <- colnames(tab)[substr(colnames(tab), 1, 3) == 'NPC']
npc_terms <- npc_terms[npc_terms != 'NPC_GaD']

#logreg_formula <- as.formula('status ~ 1 + NPC_GiGaD + NPC_GsTpD + NPC_GaDaGaD')
#logreg_formula <- as.formula('status ~ 1 + GPC_GiGaD + GPC_GsTpD + GPC_GaDaGaD')
npc_terms <- c('NPC_GiGaD', 'NPC_GsTpD', 'NPC_GaDaGaD')
#tab <- tab[apply(tab[, npc_terms] > 0, 1, any, na.rm=TRUE), ] # remove all zero pairs
tab <- tab[! apply(is.na(tab[, npc_terms]), 1, any), ] # remove rows with missing NPCs

prob.positive <- 0.01
tab$weight <- NA
tab$weight[tab$status == 0] <- (1 - prob.positive) * sum(tab$status == 1) / (prob.positive * sum(tab$status == 0))
tab$weight[tab$status == 1] <- 1

npc_terms <- c('NPC_GiGaD', 'NPC_GsTpD', 'NPC_GaDaGaD')
npc_terms <- c('DPC_GiGaD', 'DPC_GsTpD', 'DPC_GaDaGaD')
logreg_formula <- as.formula(paste('status ~ 1 + ', paste(npc_terms, collapse=' + '), sep=''))
logreg <- glm(logreg_formula, family='binomial', data=tab, weights=weight, na.action='na.exclude')
summary(logreg)

scores <- predict(logreg, type='response')
getPPV(scores, tab$status)
pred <- prediction(scores, tab$status)
auc <- performance(pred, 'auc')@y.values[[1]]
auc


getPPV <- function(scores, actual) {
  complete.indices <- ! (is.na(scores) | is.na(actual))
  scores <- scores[complete.indices]
  actual <- actual[complete.indices]
  classification <- scores > prob.positive
  number.TP <- sum(classification & actual)
  PPV <- number.TP / sum(classification)
  return(PPV)
}



pred <- prediction(scores, tab$status)
perf <- performance(pred, 'tpr', 'fpr')
cbind(perf@alpha.values[[1]], perf@y.values[[1]])

roc <- performance(pred, 'tpr', 'fpr')
prec.rec <- performance(pred, 'prec', 'rec')
auc <- performance(pred, 'auc')@y.values[[1]]
auc <- round(auc, 3)
auc
plot(0, 0, type='n', xlim=c(0, 1), ylim=c(0, 1), xlab='fpr', ylab='tpr',
  main=paste(network.id, ' ROC Curve', sep=''))
lines(0:1, 0:1, lty=2)
lines(roc@x.values[[1]], roc@y.values[[1]], col='orange')
points(roc@x.values[[1]], roc@y.values[[1]], col='orange')

plot(0, 0, type='n', xlim=c(0, 1), ylim=c(0, 1), xlab='recall', ylab='precision',
  main=paste(network.id, ' PR', sep=''))
lines(0:1, 0:1, lty=2)
lines(prec.rec@x.values[[1]], prec.rec@y.values[[1]], col='orange')
points(prec.rec@x.values[[1]], prec.rec@y.values[[1]], col='orange')


# ROC Computation and Plotting for various models
formulas.list <- list()

ll <- list()
ll[['name']] <- 'Combined'
ll[['col']] <- 'black'
ll[['formula']] <- as.formula('status ~ 1 + NPC_GiGaD + NPC_GsTpD + NPC_GaDaGaD')
formulas.list[[ll[['name']]]] <- ll

ll[['name']] <- 'PPI'
ll[['col']] <- 'blue'
ll[['formula']] <- as.formula('status ~ 1 + NPC_GiGaD')
formulas.list[[ll[['name']]]] <- ll

ll[['name']] <- 'Tissue'
ll[['col']] <- 'darkgreen'
ll[['formula']] <- as.formula('status ~ 1 + NPC_GsTpD')
formulas.list[[ll[['name']]]] <- ll

ll[['name']] <- 'Diseasome'
ll[['col']] <- 'red'
ll[['formula']] <- as.formula('status ~ 1 + NPC_GaDaGaD')
formulas.list[[ll[['name']]]] <- ll

path = file.path('/home/dhimmels/Documents/serg/ashg13/', network.id, 'ROCs.pdf')
pdf(path, width=4, height=4)
par(mar=c(2.1, 2.1, 1.5, 0.5), # margins bottom, left, top, right
  mgp=c(1.1, .25, 0), # axis title, labels, and ticks
  tcl=-0.25, # tick length
  lwd= 1.8) # line width

plot(0, 0, type='n', xlim=c(0, 1), ylim=c(0, 1), xlab='fpr', ylab='tpr',
  main='GWAS Prediction ROC Curves')
lines(0:1, 0:1, lty=2)
legend.cols <- rep(NA, length(formulas.list))
legend.labels <- rep(NA, length(formulas.list))
for (i in 1:length(formulas.list)) {
  ll <- formulas.list[[i]]
  model <- glm(ll[['formula']], family='binomial', data=tab, weights=weight, na.action='na.exclude')
  scores <- predict(model, type='response')
  rocr.prediction <- prediction(scores, tab$status)
  roc <- performance(rocr.prediction, 'tpr', 'fpr')
  auc <- performance(rocr.prediction, 'auc')@y.values[[1]]
  #auc <- getPPV(scores, tab$status)
  #auc <- performance(rocr.prediction, 'auc', fpr.stop=0.15)@y.values[[1]]
  lines(roc@x.values[[1]], roc@y.values[[1]], col=ll[['col']])
  legend.cols[[i]] <- ll[['col']]
  legend.labels[[i]] <- paste(ll[['name']], ': ', round(auc, 3), sep='')
}
legend('bottomright', lty=1, col=legend.cols, title='model: AUC', cex=0.85,
  y.intersp=.9, legend=legend.labels, bty='n')
dev.off()




path <- file.path('/home/dhimmels/Documents/serg/ashg13/', network.id, 'features-multiple-sclerosis.txt')
ms.tab <- read.delim(path)
ms.predictions <- predict(logreg, type='response', newdata=ms.tab)
ms.predictions <- round(ms.predictions, 6)
ms.pred.tab <- data.frame('gene'=ms.tab$source, 'status'=ms.tab$status, 'prior'=ms.predictions,
  'NPC_GiGaD'=ms.tab$NPC_GiGaD, 'NPC_GsTpD'=ms.tab$NPC_GsTpD, 'NPC_GaDaGaD'=ms.tab$NPC_GaDaGaD)
ms.pred.tab <- ms.pred.tab[order(ms.pred.tab$prior, decreasing=TRUE), ]
path <- file.path('/home/dhimmels/Documents/serg/ashg13/', network.id, 'ms-prior-table.txt')
write.table(ms.pred.tab, path, sep='\t', quote=FALSE, row.names=FALSE)

logreg <- glm(status ~ NPC_GiGaD, family='binomial', data=tab)



logreg <- glm(status ~ NPC_GiGaD, family='binomial', data=tab)
#logreg <- glm(status ~ NPC_GiGaD + NPC_GiGrD, family='binomial', data=tab)
#logreg <- glm(status ~ NPC_GiGaD + NPC_GiGuD + NPC_GiGdD, family='binomial', data=tab)


summary(logreg)
logreg <- glm(status ~ NPC_GiGaD, family='binomial', data=tab)
pairs(tab[, npc_terms])
cor(tab[, npc_terms], use='complete.obs')




logreg <- glm(status ~ NPC_GiGaD, family='binomial', data=tab, na.action='na.exlcude')
summary(logreg)

predicted <- predict(logreg, type='response')
observed <- tab$status
