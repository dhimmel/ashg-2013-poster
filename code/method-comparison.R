library(glmnet) # elastic net
library(kernlab) # svm


source('feature-parser.R')

learning.features.path <- '/home/dhimmels/Documents/serg/ashg13/131010-1/GaD-both/analyses/learning-edges-dwpc0.5/learning-features.txt'
prob.positive <- 0.01
percent.testing <- 1 / 3
learning.tab <- ReadFeatures(learning.features.path)
learning.tab <- AddWeight(learning.tab, prob.positive)
terms <- TermSubset(learning.tab, prefixes=c('DWPC_0.5'))

genes.per.disease <- table(subset(learning.tab, status == 1)$target)
keep.diseases <- names(genes.per.disease[genes.per.disease >= 20])
learning.tab <- subset(learning.tab, target %in% keep.diseases)

train.test.list <- SplitTesting(learning.tab, learning.tab[, 'status'])
training.tab <- train.test.list$training
testing.tab <- train.test.list$testing

training.x <- as.matrix(training.tab[, terms])
training.y <- training.tab[, 'status']
training.w <- training.tab[, 'weight']
testing.x <- as.matrix(testing.tab[, terms])
testing.y <- testing.tab[, 'status']

##################################
## GLM Logistic Regression
logit.formula <- as.formula(paste(c('status ~ 1', terms), collapse=' + '))
logit.training <- glm(logit.formula, data=training.tab, weights=weight, family=binomial())
logit.test.preds <- predict(logit.training, newdata=testing.tab, type='response')

##################################
## Elastic Net Logistic Regression

glm.alpha <- 0
enet.training <- glmnet::cv.glmnet(training.x, training.y, training.w, alpha=glm.alpha, family='binomial')
geom.mean.lambda <- exp(mean(log(c(enet.training$lambda.min, enet.training$lambda.1se))))
lambda <- enet.training$lambda.1se
enet.test.preds <- predict(enet.training, newx=testing.x, type='response', s=lambda)
coef(enet.training, s=lambda)
#coef(enet.training, s=enet.training$lambda.1se)
#y.predicted <- predict(elastic.cv, learning.x, type='response')
#y.predicted <- as.numeric(y.predicted >= prob.positive)
#accuracy <- mean(learning.y == y.predicted)

##################################
## SVM
svm.training <- kernlab::ksvm(x=training.x, y=training.y, prob.model=TRUE, kernel='rbfdot')
svm.test.preds <- predict(svm.training, newdata=testing.x, type='response')

test.preds.list <- list('logit'=logit.test.preds, 'enet'=enet.test.preds, 'svm'=svm.test.preds)

library(ROCR)
library(ggplot2)
method.roc.tab <- NULL
for (method.name in names(test.preds.list)) {
  scores <- test.preds.list[[method.name]]
  rocr.pred <- ROCR::prediction(scores, testing.y)
  auc <- ROCR::performance(rocr.pred, 'auc')@y.values[[1]]
  roc <- ROCR::performance(rocr.pred, 'tpr', 'fpr')
  roc.df <- data.frame('fpr'=roc@x.values[[1]], 'tpr'=roc@y.values[[1]], 'method'=method.name)
  method.roc.tab <- rbind(method.roc.tab, roc.df)
}

ggplot(method.roc.tab, aes(fpr, tpr, color=method)) + 
  geom_line() + geom_abline(intercept=0, slope=1) +
  coord_cartesian(xlim=c(0, 1), ylim=c(0, 1)) + ggtitle('Method ROC')




