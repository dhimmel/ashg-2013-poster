efo_id	name	Associations	Regulations
EFO_0000756	melanoma	18	7602
EFO_0000341	chronic obstructive pulmonary disease	10	1497
EFO_0000292	bladder carcinoma	16	12361
EFO_0000731	uterine fibroid	7	4236
EFO_0000676	psoriasis	22	12935
EFO_0000407	dilated cardiomyopathy	2	9712
EFO_0001365	age-related macular degeneration	24	780
EFO_0000270	asthma	30	168
EFO_0000275	atrial fibrillation	14	996
EFO_0000182	hepatocellular carcinoma	7	12015
EFO_0001360	type II diabetes mellitus	68	818
EFO_0000571	lung adenocarcinoma	9	6484
EFO_0000253	amyotrophic lateral sclerosis	15	8061
EFO_0000365	colorectal adenocarcinoma	13	15132
EFO_0002618	pancreatic carcinoma	10	9592
EFO_0001054	leprosy	7	1129
EFO_0000621	neuroblastoma	6	10130
EFO_0000384	Crohn's disease	161	5417
EFO_0000474	epilepsy	16	12677
EFO_0003898	ankylosing spondylitis	14	42
EFO_0002508	Parkinson's disease	44	728
EFO_0001073	obesity	27	2106
EFO_0000183	Hodgkins lymphoma	6	3842
EFO_0003060	non-small cell lung carcinoma	1	12793
EFO_0001663	prostate carcinoma	44	11142
EFO_0000180	HIV-1 infection	4	1938
EFO_0000660	polycystic ovary syndrome	6	426
EFO_0003897	stomach neoplasm	4	6054
EFO_0000625	nevus	2	4721
EFO_0000768	idiopathic pulmonary fibrosis	1	1
EFO_0000765	AIDS	10	2263
EFO_0000280	Barrett's esophagus	1	66
EFO_0000174	Ewing sarcoma	7	10294
EFO_0000339	chronic myelogenous leukemia	2	11023
EFO_0002690	systemic lupus erythematosus	31	1574
EFO_0000649	periodontitis	1	11753
EFO_0000729	ulcerative colitis	90	5417
EFO_0000178	gastric carcinoma	3	6054
EFO_0000220	acute lymphoblastic leukemia	7	16562
EFO_0002506	osteoarthritis	3	8355
EFO_0000681	renal cell carcinoma	3	15028
EFO_0002892	thyroid carcinoma	5	10402
EFO_0000200	plasma cell neoplasm	5	12498
EFO_0004252	nasopharyngeal neoplasm	9	5194
EFO_0000249	Alzheimer's disease	19	50
EFO_0002609	chronic childhood arthritis	1	3467
EFO_0001065	endometriosis	1	1
EFO_0003767	inflammatory bowel disease	14	5417
EFO_0000289	bipolar disorder	9	1
EFO_0000095	chronic lymphocytic leukemia	15	11122
EFO_0001378	multiple myeloma	2	12498
EFO_0001359	type I diabetes mellitus	59	818
EFO_0003885	multiple sclerosis	108	433
EFO_0001068	malaria	3	8046
