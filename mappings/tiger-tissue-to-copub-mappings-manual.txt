tiger_tissue	bi_id	copub_name
heart	203066	heart
brain	202699	brain
colon	202801	colon
skin	203533	skin
mammary_gland	203219	mammary gland
kidney	203142	kidney
cervix	202775	cervix
testis	203615	testis
eye	202961	eye
stomach	203572	stomach
soft_tissue	203547	soft tissue
bone_marrow	202693	bone marrow
peripheral_nervous_system	203408	peripheral nervous system
bladder	202679	bladder
larynx	203158	larynx
blood	203480	red blood cell
lymph_node	203196	lymph
prostate	203459	prostate
tongue	203640	tongue
pancreas	203372	pancreas
placenta	203436	placenta
spleen	203558	spleen
small_intestine	203543	small intestine
thymus	203632	thymus
uterus	203690	uterus
lung	203186	lung
ovary	203365	ovaries
muscle	203266	muscle
bone	202692	bone
liver	203179	liver
