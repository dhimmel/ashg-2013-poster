# ASHG 2013 Analysis & Poster

This code was used to create the network and analysis for a poster at ASHG 2013 in Boston (American Society of Human Genetics 63rd Annual Meeting).

The networks were created with the code at <https://github.com/dhimmel/serg-pycode/tree/main/projects/ashg13>.

The PDF poster is available at [`poster/poster.pdf`](poster/poster.pdf).

Note that Daniel Himmelstein presented a [related poster](https://f1000research.com/posters/1097367 "Heterogeneous network link prediction prioritizes disease-associated genes. F1000 Posters") at ASHG 2014 in San Diego.

## Poster 1034F

Presented on 2013-10-25.
Copied from the [poster abstracts](https://www.ashg.org/wp-content/uploads/2019/10/2013-poster-abstracts.pdf):

> **Topological analysis of heterogeneous domain knowledge networks prioritizes genes associated with complex diseases.**  
> D.S Himmelstein, S.E. Baranzini
>
> Genome Wide Association Studies (GWAS) remain the preeminent strategy for identifying disease-associated variants, although small effect sizes combined with constraints on expanded sampling limit the pace of discovery.
Gene prioritization approaches can increase the power of GWAS by increasing the prior probability of association, but generally only incorporate information from a single biological domain. Here we present a method to prioritize disease-associated genes from a heterogenous network which captures the diversity of biological entities and relationships underlying complex disease pathogenesis.
We created a network of 11,249 genes/proteins, 30 tissues, and 44 complex diseases. Using publicly available resources, we connected the three entity types:
gene-gene relationships indicate protein-protein interactions, gene-tissue relationships indicate tissue-specific gene expression;
tissue-disease relationships represent known pathogenesis;
and disease-gene relationships indicate reported associations from published GWAS studies.
By adapting an existing algorithm, PathPredict,
we provide a framework for computing the probability that a gene-disease pair represents a true association.
The method calculates the prevalence of different network topologies connecting genes, tissues, and diseases.
A logistic regression model identifies influential topological features and enables prediction of gene-disease pairs with an unknown association status.
We assessed the performance of our method on six diseases - rheumatoid arthritis, inflammatory bowel disease, multiple sclerosis (MS), ankylosing spondylitis, psoriasis, and systemic lupus erythematosus - with multiple available large-scale GWAS.
Our algorithm scored confirmed associations highly, yielding disease-specific AUC in the range of 0.70-0.79.
Using our predictions as prior probabilities, we identified candidate genes that have not been associated to date.
Relying on independent GWAS for discovery and validation, we highlight REL, JAK2, and TNFAIP3 as likely candidates in MS.
The probability of the observed validation rate occurring by random prior estimates is 0.002, underscoring our ability to uncover novel associations.
Integrating information across multiple domains through the use of heterogeneous networks significantly improved performance.
Topological analysis of heterogeneous networks provides a potentially powerful new platform for discovery in human genetics.


## Modified timestamps

This project did not use version control, until long after it's completion.
Therefore last modified timestamps are included in [readme-file-timestamps.md](readme-timestamps.md) for all files, from when the git repository was initialized:

