## Modified timestamps

This project did not use version control, until long after it's completion.
Therefore last modified timestamps are included below for all files, from when the git repository was initialized.

## Analysis files

Table created with shell command:

```bash
echo "file_name | last_modified | size_bytes
--- | --- | ---
$(git diff-tree --no-commit-id --name-only -r be925c87 | xargs --delimiter='\n' stat --format='`%n` | `%y` | %s')
" >> readme-timestamps.md
```

file_name | last_modified | size_bytes
--- | --- | ---
`120521-1/features-multiple-sclerosis.txt` | `2013-07-11 18:17:41.789754999 -0400` | 1581155
`120521-1/learning-features.txt` | `2013-07-11 18:17:41.833754999 -0400` | 1089846
`120521-1/prepared-graph.pkl` | `2013-07-11 18:17:41.905754999 -0400` | 3075546
`120521-1/raw-graph.gml` | `2013-07-11 18:17:41.969754999 -0400` | 2676746
`120521-1/raw-graph.pkl` | `2013-07-11 18:17:42.061754999 -0400` | 2551354
`130520-1/features-multiple-sclerosis.txt` | `2013-07-11 18:17:42.141754999 -0400` | 3297843
`130520-1/learning-features.txt` | `2013-07-11 18:17:42.189754999 -0400` | 703222
`130520-1/prepared-graph.pkl` | `2013-07-11 18:17:45.553754999 -0400` | 91221218
`130520-1/raw-graph.gml` | `2013-07-11 18:17:47.841754999 -0400` | 108061294
`130520-1/raw-graph.pkl` | `2013-07-11 18:17:50.453754999 -0400` | 90888926
`130521-2/learning-features.txt` | `2013-07-11 18:17:50.713754999 -0400` | 397245
`130521-2/prepared-graph.pkl` | `2013-07-11 18:17:50.837754999 -0400` | 8183209
`130521-2/raw-graph.gml` | `2013-07-11 18:17:50.989754999 -0400` | 7761951
`130521-2/raw-graph.pkl` | `2013-07-11 18:17:51.121754999 -0400` | 7658630
`130523-2/ROCs.pdf` | `2013-07-11 18:17:51.133754999 -0400` | 13454
`130523-2/features-multiple-sclerosis.txt` | `2013-07-11 18:17:51.209754999 -0400` | 2318713
`130523-2/features/disease/Alzheimer's disease.txt` | `2013-07-11 18:44:10.993754999 -0400` | 2112326
`130523-2/features/disease/Behcet's syndrome.txt` | `2013-07-11 18:44:11.025754999 -0400` | 1933523
`130523-2/features/disease/Crohn's disease.txt` | `2013-07-11 18:44:11.069754999 -0400` | 2312496
`130523-2/features/disease/Dupuytren Contracture.txt` | `2013-07-11 18:44:11.113754999 -0400` | 2121662
`130523-2/features/disease/Graves disease.txt` | `2013-07-11 18:44:11.261754999 -0400` | 1943498
`130523-2/features/disease/Parkinson's disease.txt` | `2013-07-11 18:44:11.305754999 -0400` | 2197196
`130523-2/features/disease/Progressive supranuclear palsy.txt` | `2013-07-11 18:44:11.345754999 -0400` | 2289327
`130523-2/features/disease/Vitiligo.txt` | `2013-07-11 18:44:11.385754999 -0400` | 2088804
`130523-2/features/disease/age-related macular degeneration.txt` | `2013-07-11 18:44:11.449754999 -0400` | 2266117
`130523-2/features/disease/amyotrophic lateral sclerosis.txt` | `2013-07-11 18:44:11.493754999 -0400` | 2239806
`130523-2/features/disease/androgenetic alopecia.txt` | `2013-07-11 18:44:11.525754999 -0400` | 2224342
`130523-2/features/disease/ankylosing spondylitis.txt` | `2013-07-11 18:44:11.573754999 -0400` | 2135509
`130523-2/features/disease/asthma.txt` | `2013-07-11 18:44:11.605754999 -0400` | 2043193
`130523-2/features/disease/atopic eczema.txt` | `2013-07-11 18:44:11.645754999 -0400` | 2096947
`130523-2/features/disease/atrial fibrillation.txt` | `2013-07-11 18:44:11.693754999 -0400` | 2085129
`130523-2/features/disease/attention deficit hyperactivity disorder.txt` | `2013-07-11 18:44:11.737754999 -0400` | 2151015
`130523-2/features/disease/biliary liver cirrhosis.txt` | `2013-07-11 18:44:11.789754999 -0400` | 2245461
`130523-2/features/disease/bipolar disorder.txt` | `2013-07-11 18:44:11.853754999 -0400` | 2069690
`130523-2/features/disease/brain aneurysm.txt` | `2013-07-11 18:44:11.905754999 -0400` | 2064955
`130523-2/features/disease/cardiac hypertrophy.txt` | `2013-07-11 18:44:11.953754999 -0400` | 2051882
`130523-2/features/disease/celiac disease.txt` | `2013-07-11 18:44:11.997754999 -0400` | 2181441
`130523-2/features/disease/chronic obstructive pulmonary disease.txt` | `2013-07-11 18:44:12.061754999 -0400` | 2313120
`130523-2/features/disease/coronary heart disease.txt` | `2013-07-11 18:44:12.125754999 -0400` | 2216782
`130523-2/features/disease/dental caries.txt` | `2013-07-11 18:44:12.181754999 -0400` | 2024613
`130523-2/features/disease/epilepsy.txt` | `2013-07-11 18:44:12.213754999 -0400` | 1893150
`130523-2/features/disease/glaucoma.txt` | `2013-07-11 18:44:12.249754999 -0400` | 1915641
`130523-2/features/disease/hypertension.txt` | `2013-07-11 18:44:12.285754999 -0400` | 2072468
`130523-2/features/disease/hypothyroidism.txt` | `2013-07-11 18:44:12.337754999 -0400` | 2177181
`130523-2/features/disease/inflammatory bowel disease.txt` | `2013-07-11 18:44:12.413754999 -0400` | 2519182
`130523-2/features/disease/kidney disease.txt` | `2013-07-11 18:44:12.453754999 -0400` | 2190073
`130523-2/features/disease/migraine disorder.txt` | `2013-07-11 18:44:12.493754999 -0400` | 2071346
`130523-2/features/disease/mucocutaneous lymph node syndrome.txt` | `2013-07-11 18:44:12.541754999 -0400` | 2357878
`130523-2/features/disease/multiple sclerosis.txt` | `2013-07-11 18:43:56.261754999 -0400` | 2318713
`130523-2/features/disease/myocardial infarction.txt` | `2013-07-11 18:44:12.569754999 -0400` | 2123258
`130523-2/features/disease/myopia.txt` | `2013-07-11 18:44:12.613754999 -0400` | 1978534
`130523-2/features/disease/obesity.txt` | `2013-07-11 18:44:12.673754999 -0400` | 2101720
`130523-2/features/disease/psoriasis.txt` | `2013-07-11 18:44:12.733754999 -0400` | 2089804
`130523-2/features/disease/rheumatoid arthritis.txt` | `2013-07-11 18:44:12.769754999 -0400` | 2233504
`130523-2/features/disease/schizophrenia.txt` | `2013-07-11 18:44:12.813754999 -0400` | 2070636
`130523-2/features/disease/systemic lupus erythematosus.txt` | `2013-07-11 18:44:12.873754999 -0400` | 2393618
`130523-2/features/disease/systemic scleroderma.txt` | `2013-07-11 18:44:12.929754999 -0400` | 2229817
`130523-2/features/disease/type I diabetes mellitus.txt` | `2013-07-11 18:44:12.985754999 -0400` | 2323106
`130523-2/features/disease/type II diabetes mellitus.txt` | `2013-07-11 18:44:13.025754999 -0400` | 2360365
`130523-2/features/disease/ulcerative colitis.txt` | `2013-07-11 18:44:13.073754999 -0400` | 2276180
`130523-2/features/learning-features.txt` | `2013-07-11 18:34:23.925754999 -0400` | 942354
`130523-2/graphics/disease-specific-ROCs.pdf` | `2013-07-11 18:35:17.545754999 -0400` | 63596
`130523-2/graphics/global-ROCs.pdf` | `2013-07-17 20:46:55.757685813 -0400` | 18005
`130523-2/graphics/gwas-integrations.pdf` | `2013-07-11 18:35:17.557754999 -0400` | 19886
`130523-2/integrations/age-related macular degeneration/all-genes-topinblock-marginal-in-phs000001.pha002856.txt` | `2013-07-11 18:44:13.089754999 -0400` | 41389
`130523-2/integrations/age-related macular degeneration/all-genes-topinblock-marginal-in-phs000182.pha002890.txt` | `2013-07-11 18:44:13.089754999 -0400` | 31728
`130523-2/integrations/age-related macular degeneration/all-genes.txt` | `2013-07-11 18:44:13.125754999 -0400` | 873780
`130523-2/integrations/age-related macular degeneration/novel-genes-topinblock-marginal-in-phs000001.pha002856.txt` | `2013-07-11 18:44:13.125754999 -0400` | 40854
`130523-2/integrations/age-related macular degeneration/novel-genes-topinblock-marginal-in-phs000182.pha002890.txt` | `2013-07-11 18:44:13.129754999 -0400` | 30423
`130523-2/integrations/age-related macular degeneration/novel-genes.txt` | `2013-07-11 18:44:13.145754999 -0400` | 783211
`130523-2/integrations/amyotrophic lateral sclerosis/all-genes-topinblock-marginal-in-phs000101.pha002846.txt` | `2013-07-11 18:44:13.149754999 -0400` | 32416
`130523-2/integrations/amyotrophic lateral sclerosis/all-genes-topinblock-marginal-in-phs000127.pha002851.txt` | `2013-07-11 18:44:13.165754999 -0400` | 30438
`130523-2/integrations/amyotrophic lateral sclerosis/all-genes.txt` | `2013-07-11 18:44:13.185754999 -0400` | 867911
`130523-2/integrations/amyotrophic lateral sclerosis/novel-genes-topinblock-marginal-in-phs000101.pha002846.txt` | `2013-07-11 18:44:13.185754999 -0400` | 32311
`130523-2/integrations/amyotrophic lateral sclerosis/novel-genes-topinblock-marginal-in-phs000127.pha002851.txt` | `2013-07-11 18:44:13.189754999 -0400` | 30261
`130523-2/integrations/amyotrophic lateral sclerosis/novel-genes.txt` | `2013-07-11 18:44:13.209754999 -0400` | 790050
`130523-2/integrations/multiple sclerosis/all-genes-topinblock-marginal-in-phs000139.pha002854.txt` | `2013-07-11 18:44:13.213754999 -0400` | 35033
`130523-2/integrations/multiple sclerosis/all-genes-topinblock-marginal-in-phs000171.pha002861.txt` | `2013-07-11 18:44:13.221754999 -0400` | 28874
`130523-2/integrations/multiple sclerosis/all-genes.txt` | `2013-07-11 18:44:13.245754999 -0400` | 889854
`130523-2/integrations/multiple sclerosis/novel-genes-topinblock-marginal-in-phs000139.pha002854.txt` | `2013-07-11 18:44:13.245754999 -0400` | 32663
`130523-2/integrations/multiple sclerosis/novel-genes-topinblock-marginal-in-phs000171.pha002861.txt` | `2013-07-11 18:44:13.245754999 -0400` | 26752
`130523-2/integrations/multiple sclerosis/novel-genes.txt` | `2013-07-11 18:44:13.273754999 -0400` | 785891
`130523-2/integrations/systemic lupus erythematosus/all-genes-topinblock-marginal-in-phs000122.pha002848.txt` | `2013-07-11 18:44:13.281754999 -0400` | 37779
`130523-2/integrations/systemic lupus erythematosus/all-genes-topinblock-marginal-in-phs000216.pha002867.txt` | `2013-07-11 18:44:13.285754999 -0400` | 37998
`130523-2/integrations/systemic lupus erythematosus/all-genes.txt` | `2013-07-11 18:44:13.301754999 -0400` | 904288
`130523-2/integrations/systemic lupus erythematosus/novel-genes-topinblock-marginal-in-phs000122.pha002848.txt` | `2013-07-11 18:44:13.301754999 -0400` | 36359
`130523-2/integrations/systemic lupus erythematosus/novel-genes-topinblock-marginal-in-phs000216.pha002867.txt` | `2013-07-11 18:44:13.305754999 -0400` | 35948
`130523-2/integrations/systemic lupus erythematosus/novel-genes.txt` | `2013-07-11 18:44:13.325754999 -0400` | 807116
`130523-2/integrations/type I diabetes mellitus/all-genes-topinblock-marginal-in-phs000018.pha002864.txt` | `2013-07-11 18:44:13.329754999 -0400` | 34190
`130523-2/integrations/type I diabetes mellitus/all-genes-topinblock-marginal-in-phs000180.pha002862.txt` | `2013-07-11 18:44:13.329754999 -0400` | 45881
`130523-2/integrations/type I diabetes mellitus/all-genes.txt` | `2013-07-11 18:44:13.353754999 -0400` | 890621
`130523-2/integrations/type I diabetes mellitus/novel-genes-topinblock-marginal-in-phs000018.pha002864.txt` | `2013-07-11 18:44:13.357754999 -0400` | 33326
`130523-2/integrations/type I diabetes mellitus/novel-genes-topinblock-marginal-in-phs000180.pha002862.txt` | `2013-07-11 18:44:13.361754999 -0400` | 43507
`130523-2/integrations/type I diabetes mellitus/novel-genes.txt` | `2013-07-11 18:44:13.393754999 -0400` | 783446
`130523-2/learning-features.txt` | `2013-07-11 18:17:51.241754999 -0400` | 942354
`130523-2/ms-posterior-novel-table-meta0.05.txt` | `2013-07-11 18:17:51.241754999 -0400` | 6748
`130523-2/ms-posterior-novel-table-wtccc0.05.txt` | `2013-07-11 18:17:51.241754999 -0400` | 10648
`130523-2/ms-posterior-novel-table.txt` | `2013-07-11 18:17:51.245754999 -0400` | 62295
`130523-2/ms-posterior-table.txt` | `2013-07-11 18:17:51.261754999 -0400` | 68296
`130523-2/ms-prior-table.txt` | `2013-07-11 18:17:51.269754999 -0400` | 194040
`130523-2/partial-ROCs.pdf` | `2013-07-11 18:17:51.269754999 -0400` | 8335
`130523-2/prepared-graph.pkl` | `2013-07-11 18:17:51.349754999 -0400` | 3502726
`130523-2/raw-graph.gml` | `2013-07-11 18:17:51.429754999 -0400` | 3341937
`130523-2/raw-graph.pkl` | `2013-07-11 18:17:51.501754999 -0400` | 3224083
`130530-1/MS-LD-novel-marginal-in-meta2.5.txt` | `2013-07-11 18:17:51.509754999 -0400` | 74012
`130530-1/MS-LD-novel-marginal-in-meta2.5.xlsx` | `2013-07-11 18:17:51.521754999 -0400` | 56743
`130530-1/MS-LD-novel-marginal-in-wtccc2.txt` | `2013-07-11 18:17:51.521754999 -0400` | 13089
`130530-1/MS-novel-marginal-in-meta2.5.txt` | `2013-07-11 18:17:51.525754999 -0400` | 30464
`130530-1/MS-novel-marginal-in-wtccc2.txt` | `2013-07-11 18:17:51.541754999 -0400` | 26164
`130530-1/MS-novel.txt` | `2013-07-11 18:17:51.573754999 -0400` | 960131
`130530-1/MS-novel.xlsx` | `2013-07-11 18:17:51.621754999 -0400` | 602983
`130530-1/ROCs-WTCCC2-Gold-Standard.pdf` | `2013-07-11 18:17:51.621754999 -0400` | 7196
`130530-1/ROCs.pdf` | `2013-07-11 18:17:51.637754999 -0400` | 12438
`130530-1/features-multiple-sclerosis.txt` | `2013-07-11 18:17:51.693754999 -0400` | 2194216
`130530-1/learning-features.txt` | `2013-07-11 18:17:51.717754999 -0400` | 901420
`130530-1/ms-genes-novel-meta0.05-nodup.txt` | `2013-07-11 18:17:51.753754999 -0400` | 4080
`130530-1/ms-genes-novel-meta0.05.txt` | `2013-07-11 18:17:51.753754999 -0400` | 7775
`130530-1/ms-genes-novel.txt` | `2013-07-11 18:17:51.785754999 -0400` | 421725
`130530-1/ms-genes.txt` | `2013-07-11 18:17:51.829754999 -0400` | 436803
`130530-1/ms-prior-table.txt` | `2013-07-11 18:17:51.853754999 -0400` | 283139
`130530-1/prepared-graph.pkl` | `2013-07-11 18:17:51.941754999 -0400` | 3482491
`130530-1/raw-graph.gml` | `2013-07-11 18:17:52.013754999 -0400` | 3335062
`130530-1/raw-graph.pkl` | `2013-07-11 18:17:52.089754999 -0400` | 3217561
`130717-1/description.txt` | `2013-07-17 18:41:13.001807819 -0400` | 144
`130717-1/features/learning-features-damp-0.5.txt` | `2013-07-17 19:30:30.529759980 -0400` | 1102174
`130717-1/features/learning-features-damp-1.0.txt` | `2013-07-17 19:11:00.417778907 -0400` | 1106579
`130717-1/features/learning-features.txt` | `2013-07-19 20:11:27.274925165 -0400` | 2583683
`130717-1/graphics/feature-comparison-ROC.pdf` | `2013-07-26 22:51:10.202421000 -0400` | 38371
`130717-1/graphics/global-ROCs-DPC-0.5.pdf` | `2013-07-17 21:10:15.553663171 -0400` | 21279
`130717-1/graphics/global-ROCs-DPC-1.0.pdf` | `2013-07-17 21:09:44.413663675 -0400` | 21353
`130717-1/graphics/global-ROCs-NPC.pdf` | `2013-07-17 21:12:10.373661314 -0400` | 21923
`130717-1/prepared-graph.pkl` | `2013-07-11 18:17:51.349754000 -0400` | 3502726
`130726-1/features/learning-features.txt` | `2013-07-26 23:05:32.198421000 -0400` | 3142815
`130726-1/graphics/feature-comparison-ROC.pdf` | `2013-07-27 17:33:44.334421000 -0400` | 40115
`130726-1/prepared-graph.pkl` | `2013-07-26 22:36:03.618421000 -0400` | 4236667
`130726-1/raw-graph.gml` | `2013-07-26 22:35:20.422421000 -0400` | 3902487
`130726-1/raw-graph.pkl` | `2013-07-26 22:35:20.942421000 -0400` | 3893295
`130727-1/features/learning-features.txt` | `2013-07-27 20:22:18.050421000 -0400` | 4376450
`130727-1/graphics/feature-comparison-ROC.pdf` | `2013-07-27 20:22:47.702421000 -0400` | 41249
`130727-1/graphics/performance.pdf` | `2013-07-29 19:49:46.758421000 -0400` | 32351
`130727-1/network_creation.log` | `2013-07-27 19:31:50.710421000 -0400` | 691
`130727-1/prepared-graph.pkl` | `2013-07-27 19:45:23.510421000 -0400` | 5398829
`130727-1/raw-graph.gml` | `2013-07-27 19:31:53.466421000 -0400` | 5150942
`130727-1/raw-graph.pkl` | `2013-07-27 19:31:54.290421000 -0400` | 5084420
`130730-1/features/learning-features.txt` | `2013-07-31 20:08:42.766421000 -0400` | 6265289
`130730-1/graphics/performance.pdf` | `2013-07-31 22:23:20.462421000 -0400` | 49960
`130730-1/network_creation.log` | `2013-07-30 19:49:04.946421000 -0400` | 1646
`130730-1/prepared-graph.pkl` | `2013-07-30 19:50:37.262421000 -0400` | 34717313
`130730-1/raw-graph.gml` | `2013-07-30 19:49:29.226421000 -0400` | 34542181
`130730-1/raw-graph.pkl` | `2013-07-30 19:49:37.810421000 -0400` | 36042151
`130814-1/GaD-both/learning-edges/matched-negative-1s-1t.txt` | `2013-08-27 14:35:46.452818866 -0400` | 1300931
`130814-1/GaD-both/metaedge.txt` | `2013-08-26 16:40:20.004265018 -0400` | 42
`130814-1/GaD-both/metapaths/length-2-cutoff.txt` | `2013-09-03 06:58:02.160870187 -0400` | 362
`130814-1/GaD-both/metapaths/length-3-cutoff.txt` | `2013-09-03 06:58:02.160870187 -0400` | 1797
`130814-1/GaD-both/metapaths/length-4-cutoff.txt` | `2013-08-26 18:01:04.988299017 -0400` | 7471
`130814-1/features/all-features.txt` | `2013-09-03 06:58:02.212870187 -0400` | 523
`130814-1/features/dwpc_0.5.txt` | `2013-09-03 06:58:02.212870187 -0400` | 65
`130814-1/graph/creation.log` | `2013-08-21 19:32:25.321719892 -0400` | 212
`130814-1/graph/graph.cfg` | `2013-08-15 20:35:06.246108531 -0400` | 567
`130814-1/graph/graph.json.gz` | `2013-08-21 19:35:22.449721135 -0400` | 5378169
`130814-1/graph/metagraph.json` | `2013-08-21 19:33:25.133720312 -0400` | 597
`130904-1/GaD-both/analyses/all-features/arguments.cfg` | `2013-09-04 21:45:03.229849946 -0400` | 209
`130904-1/GaD-both/analyses/all-features/learning-features.txt` | `2013-09-05 11:04:19.054186464 -0400` | 6516094
`130904-1/GaD-both/analyses/dwpc_0.5-small/arguments.cfg` | `2013-09-05 16:39:46.866327707 -0400` | 205
`130904-1/GaD-both/analyses/dwpc_0.5-small/learning-features.txt` | `2013-09-05 22:30:52.986475533 -0400` | 290123
`130904-1/GaD-both/analyses/dwpc_0.5-small/profile` | `2013-09-06 00:37:15.230528740 -0400` | 124993
`130904-1/GaD-both/analyses/dwpc_0.5-small/profile.txt` | `2013-09-05 22:30:53.006475534 -0400` | 124842
`130904-1/GaD-both/analyses/dwpc_0.5/arguments.cfg` | `2013-09-04 21:46:43.197850647 -0400` | 205
`130904-1/GaD-both/analyses/dwpc_0.5/learning-features.txt` | `2013-09-07 12:11:26.103427308 -0400` | 2747293
`130904-1/GaD-both/analyses/trial3/arguments.cfg` | `2013-09-05 21:13:56.878443141 -0400` | 205
`130904-1/GaD-both/analyses/trial3/learning-features.txt` | `2013-09-06 00:37:15.214528740 -0400` | 908192
`130904-1/GaD-both/learning-edges/matched-negative-1s-1t.txt` | `2013-09-04 19:04:40.165782418 -0400` | 1300931
`130904-1/GaD-both/metaedge.txt` | `2013-09-04 19:04:39.037782410 -0400` | 42
`130904-1/GaD-both/metapaths/length-2-cutoff.txt` | `2013-09-04 19:04:39.041782410 -0400` | 362
`130904-1/GaD-both/metapaths/length-3-cutoff.txt` | `2013-09-04 19:04:39.045782410 -0400` | 1797
`130904-1/GaD-both/metapaths/length-4-cutoff.txt` | `2013-09-04 19:04:39.061782410 -0400` | 7471
`130904-1/features/all-features.txt` | `2013-09-04 19:04:40.169782418 -0400` | 523
`130904-1/features/dwpc_0.5.txt` | `2013-09-04 17:23:32.725739841 -0400` | 77
`130904-1/graph/creation.log` | `2013-09-04 19:07:42.565783698 -0400` | 212
`130904-1/graph/graph.cfg` | `2013-09-04 15:59:25.593704424 -0400` | 692
`130904-1/graph/graph.json.gz` | `2013-09-04 19:13:42.073786221 -0400` | 5422516
`130904-1/graph/graph.pkl.gz` | `2013-09-04 19:10:02.457784679 -0400` | 17926597
`130904-1/graph/graph.yaml.gz` | `2013-09-04 19:12:10.837785580 -0400` | 4964913
`131010-1/GaD-both/analyses/DOID:0050589/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 186
`131010-1/GaD-both/analyses/DOID:0050589/learning-features.txt` | `2013-10-19 11:49:28.000000000 -0400` | 10713467
`131010-1/GaD-both/analyses/DOID:10286/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10286/learning-features.txt` | `2013-10-20 08:10:57.000000000 -0400` | 9688218
`131010-1/GaD-both/analyses/DOID:1040/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:1040/learning-features.txt` | `2013-10-19 05:02:12.000000000 -0400` | 9661613
`131010-1/GaD-both/analyses/DOID:10608/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10608/learning-features.txt` | `2013-10-19 07:14:43.000000000 -0400` | 10423778
`131010-1/GaD-both/analyses/DOID:10652/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10652/learning-features.txt` | `2013-10-19 04:32:37.000000000 -0400` | 9837748
`131010-1/GaD-both/analyses/DOID:10763/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10763/learning-features.txt` | `2013-10-20 10:10:36.000000000 -0400` | 9886861
`131010-1/GaD-both/analyses/DOID:10871/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10871/learning-features.txt` | `2013-10-18 21:02:02.000000000 -0400` | 6902805
`131010-1/GaD-both/analyses/DOID:10941/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:10941/learning-features.txt` | `2013-10-19 11:41:12.000000000 -0400` | 9640873
`131010-1/GaD-both/analyses/DOID:1107/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:1107/learning-features.txt` | `2013-10-19 12:36:40.000000000 -0400` | 9304880
`131010-1/GaD-both/analyses/DOID:11830/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:11830/learning-features.txt` | `2013-10-18 19:25:31.000000000 -0400` | 6648664
`131010-1/GaD-both/analyses/DOID:12236/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:12236/learning-features.txt` | `2013-10-19 06:44:00.000000000 -0400` | 10293928
`131010-1/GaD-both/analyses/DOID:12306/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:12306/learning-features.txt` | `2013-10-19 10:24:55.000000000 -0400` | 10145141
`131010-1/GaD-both/analyses/DOID:12361/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:12361/learning-features.txt` | `2013-10-19 13:31:52.000000000 -0400` | 9952122
`131010-1/GaD-both/analyses/DOID:13241/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:13241/learning-features.txt` | `2013-10-19 04:15:05.000000000 -0400` | 9415517
`131010-1/GaD-both/analyses/DOID:13378/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:13378/learning-features.txt` | `2013-10-19 12:18:21.000000000 -0400` | 9793605
`131010-1/GaD-both/analyses/DOID:14221/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:14221/learning-features.txt` | `2013-10-21 03:56:09.000000000 -0400` | 10059669
`131010-1/GaD-both/analyses/DOID:14330/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 184
`131010-1/GaD-both/analyses/DOID:14330/learning-features.txt` | `2013-10-19 04:30:09.000000000 -0400` | 10065117
`131010-1/GaD-both/analyses/DOID:1459/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:1459/learning-features.txt` | `2013-10-20 13:30:35.000000000 -0400` | 9304730
`131010-1/GaD-both/analyses/DOID:1826/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:1826/learning-features.txt` | `2013-10-19 01:36:24.000000000 -0400` | 9519003
`131010-1/GaD-both/analyses/DOID:1909/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:1909/learning-features.txt` | `2013-10-21 08:01:25.000000000 -0400` | 8365963
`131010-1/GaD-both/analyses/DOID:216/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:216/learning-features.txt` | `2013-10-18 19:56:27.000000000 -0400` | 6128662
`131010-1/GaD-both/analyses/DOID:2377/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:2377/learning-features.txt` | `2013-10-19 10:15:20.000000000 -0400` | 10290596
`131010-1/GaD-both/analyses/DOID:2468/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:2468/learning-features.txt` | `2013-10-19 01:19:39.000000000 -0400` | 9409619
`131010-1/GaD-both/analyses/DOID:2841/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:2841/learning-features.txt` | `2013-10-19 11:25:07.000000000 -0400` | 9556299
`131010-1/GaD-both/analyses/DOID:2914/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:2914/learning-features.txt` | `2013-10-20 16:38:05.000000000 -0400` | 10175518
`131010-1/GaD-both/analyses/DOID:3083/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:3083/learning-features.txt` | `2013-10-20 00:46:09.000000000 -0400` | 9114646
`131010-1/GaD-both/analyses/DOID:3310/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:3310/learning-features.txt` | `2013-10-19 11:57:16.000000000 -0400` | 8054343
`131010-1/GaD-both/analyses/DOID:332/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:332/learning-features.txt` | `2013-10-19 01:12:59.000000000 -0400` | 9661254
`131010-1/GaD-both/analyses/DOID:3393/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:3393/learning-features.txt` | `2013-10-19 06:04:42.000000000 -0400` | 9939110
`131010-1/GaD-both/analyses/DOID:3459/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:3459/learning-features.txt` | `2013-10-19 16:37:36.000000000 -0400` | 8515669
`131010-1/GaD-both/analyses/DOID:3905/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:3905/learning-features.txt` | `2013-10-20 09:47:50.000000000 -0400` | 9454047
`131010-1/GaD-both/analyses/DOID:4007/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:4007/learning-features.txt` | `2013-10-21 03:13:36.000000000 -0400` | 9383139
`131010-1/GaD-both/analyses/DOID:4905/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:4905/learning-features.txt` | `2013-10-20 12:40:10.000000000 -0400` | 8712500
`131010-1/GaD-both/analyses/DOID:5419/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:5419/learning-features.txt` | `2013-10-19 01:15:33.000000000 -0400` | 9482541
`131010-1/GaD-both/analyses/DOID:557/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:557/learning-features.txt` | `2013-10-19 14:15:32.000000000 -0400` | 9334975
`131010-1/GaD-both/analyses/DOID:5844/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:5844/learning-features.txt` | `2013-10-19 03:37:21.000000000 -0400` | 9577148
`131010-1/GaD-both/analyses/DOID:635/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:635/learning-features.txt` | `2013-10-18 23:21:53.000000000 -0400` | 8488059
`131010-1/GaD-both/analyses/DOID:6364/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:6364/learning-features.txt` | `2013-10-19 00:04:06.000000000 -0400` | 9145345
`131010-1/GaD-both/analyses/DOID:678/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:678/learning-features.txt` | `2013-10-19 01:20:20.000000000 -0400` | 9153775
`131010-1/GaD-both/analyses/DOID:7147/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:7147/learning-features.txt` | `2013-10-19 06:09:36.000000000 -0400` | 9467886
`131010-1/GaD-both/analyses/DOID:7148/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:7148/learning-features.txt` | `2013-10-19 09:15:26.000000000 -0400` | 9509573
`131010-1/GaD-both/analyses/DOID:8577/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:8577/learning-features.txt` | `2013-10-19 18:09:20.000000000 -0400` | 10607422
`131010-1/GaD-both/analyses/DOID:8778/arguments.cfg` | `2013-10-18 16:22:41.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:8778/learning-features.txt` | `2013-10-19 17:45:25.000000000 -0400` | 10651538
`131010-1/GaD-both/analyses/DOID:8893/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:8893/learning-features.txt` | `2013-10-19 07:55:11.000000000 -0400` | 8274882
`131010-1/GaD-both/analyses/DOID:9074/arguments.cfg` | `2013-10-18 16:22:39.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:9074/learning-features.txt` | `2013-10-19 15:48:11.000000000 -0400` | 10351658
`131010-1/GaD-both/analyses/DOID:9352/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:9352/learning-features.txt` | `2013-10-19 23:53:20.000000000 -0400` | 10274640
`131010-1/GaD-both/analyses/DOID:9744/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:9744/learning-features.txt` | `2013-10-19 22:58:19.000000000 -0400` | 10306481
`131010-1/GaD-both/analyses/DOID:986/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 182
`131010-1/GaD-both/analyses/DOID:986/learning-features.txt` | `2013-10-20 09:40:51.000000000 -0400` | 10057960
`131010-1/GaD-both/analyses/DOID:9970/arguments.cfg` | `2013-10-18 16:22:40.000000000 -0400` | 183
`131010-1/GaD-both/analyses/DOID:9970/learning-features.txt` | `2013-10-20 02:44:49.000000000 -0400` | 10031344
`131010-1/GaD-both/analyses/learning-edges-all-features/arguments.cfg` | `2013-10-11 20:02:22.222760318 -0400` | 200
`131010-1/GaD-both/analyses/learning-edges-all-features/learning-features.txt` | `2013-11-04 13:13:40.660706797 -0500` | 20372049
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5-NONTREE/arguments.cfg` | `2013-10-11 01:58:44.894161000 -0400` | 196
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5-NONTREE/learning-features.txt` | `2013-12-18 19:29:43.989053924 -0500` | 20596
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5-TREE/arguments.cfg` | `2013-10-11 01:58:44.894161000 -0400` | 196
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5-TREE/learning-features.txt` | `2013-12-18 19:33:37.168133381 -0500` | 20596
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5/arguments.cfg` | `2013-10-11 01:58:44.894161196 -0400` | 196
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5/graphics/method-comparison.pdf` | `2013-10-15 19:34:39.498998565 -0400` | 22184
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5/graphics/performance.pdf` | `2013-10-11 13:38:59.702313125 -0400` | 66386
`131010-1/GaD-both/analyses/learning-edges-dwpc0.5/learning-features.txt` | `2013-10-11 12:01:26.567986968 -0400` | 3539533
`131010-1/GaD-both/graphics/auc-scatterplot.pdf` | `2014-01-29 11:53:47.504677185 -0500` | 5672
`131010-1/GaD-both/graphics/disease-performance-train-on-all.txt` | `2014-01-29 13:38:07.485795031 -0500` | 5889
`131010-1/GaD-both/graphics/disease-specific-performance.txt` | `2013-10-21 13:37:12.634950575 -0400` | 2678
`131010-1/GaD-both/graphics/metaedge-exclusion-performance.pdf` | `2013-10-21 17:42:35.814676422 -0400` | 22076
`131010-1/GaD-both/graphics/metaedge-exclusion.txt` | `2013-10-21 13:37:32.038489097 -0400` | 6145
`131010-1/GaD-both/graphics/metapath-addition.txt` | `2013-10-21 13:37:32.042489002 -0400` | 4978
`131010-1/GaD-both/graphics/metapath-performance-2.pdf` | `2014-01-29 12:29:26.006923834 -0500` | 19070
`131010-1/GaD-both/graphics/metapath-performance-including-full.pdf` | `2013-10-21 13:43:25.278092521 -0400` | 22124
`131010-1/GaD-both/graphics/metapath-performance.pdf` | `2013-10-21 13:38:31.061085519 -0400` | 19244
`131010-1/GaD-both/graphics/model-performance-2.pdf` | `2014-01-29 12:36:11.278141686 -0500` | 10560
`131010-1/GaD-both/learning-edges/DOID:0050589.txt` | `2013-10-18 01:44:43.047492424 -0400` | 1162249
`131010-1/GaD-both/learning-edges/DOID:10286.txt` | `2013-10-18 01:44:37.599615771 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:1040.txt` | `2013-10-18 01:44:14.888129960 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:10608.txt` | `2013-10-18 01:43:43.308844833 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:10652.txt` | `2013-10-18 01:44:36.563639226 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:10763.txt` | `2013-10-18 01:44:01.608430589 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:10871.txt` | `2013-10-18 01:44:48.379371700 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:10941.txt` | `2013-10-18 01:44:45.063446779 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:1107.txt` | `2013-10-18 01:44:49.363349420 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:11830.txt` | `2013-10-18 01:43:42.356866381 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:12236.txt` | `2013-10-18 01:43:46.236778555 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:12306.txt` | `2013-10-18 01:44:00.604453317 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:12361.txt` | `2013-10-18 01:44:29.211805677 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:13241.txt` | `2013-10-18 01:44:08.780268234 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:13378.txt` | `2013-10-18 01:44:23.987923946 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:14221.txt` | `2013-10-18 01:44:22.023968409 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:14330.txt` | `2013-10-18 01:44:04.552363945 -0400` | 1124073
`131010-1/GaD-both/learning-edges/DOID:1459.txt` | `2013-10-18 01:44:09.748246319 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:1826.txt` | `2013-10-18 01:44:35.511663044 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:1909.txt` | `2013-10-18 01:43:45.236801191 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:216.txt` | `2013-10-18 01:44:16.856085407 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:2377.txt` | `2013-10-18 01:44:30.215782947 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:2468.txt` | `2013-10-18 01:44:23.007946132 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:2841.txt` | `2013-10-18 01:44:02.596408224 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:2914.txt` | `2013-10-18 01:43:54.336595204 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:3083.txt` | `2013-10-18 01:43:47.208756553 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:3310.txt` | `2013-10-18 01:44:52.323282400 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:332.txt` | `2013-10-18 01:44:40.979539246 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:3393.txt` | `2013-10-18 01:44:50.347327141 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:3459.txt` | `2013-10-18 01:44:03.576386039 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:3905.txt` | `2013-10-18 01:44:15.856108045 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:4007.txt` | `2013-10-18 01:44:42.023515607 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:4905.txt` | `2013-10-18 01:44:17.840063130 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:5419.txt` | `2013-10-18 01:44:51.331304861 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:557.txt` | `2013-10-18 01:44:27.219850775 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:5844.txt` | `2013-10-18 01:44:28.215828226 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:635.txt` | `2013-10-18 01:44:31.203760578 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:6364.txt` | `2013-10-18 01:43:48.164734914 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:678.txt` | `2013-10-18 01:43:44.272823011 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:7147.txt` | `2013-10-18 01:43:56.356549477 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:7148.txt` | `2013-10-18 01:43:57.356526841 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:8577.txt` | `2013-10-18 01:44:44.079469059 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:8778.txt` | `2013-10-18 01:44:34.467686681 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:8893.txt` | `2013-10-18 01:44:10.732224044 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:9074.txt` | `2013-10-18 01:43:49.128713092 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:9352.txt` | `2013-10-18 01:44:21.023991048 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:9744.txt` | `2013-10-18 01:44:11.712201859 -0400` | 1104985
`131010-1/GaD-both/learning-edges/DOID:986.txt` | `2013-10-18 01:44:07.808290238 -0400` | 1085897
`131010-1/GaD-both/learning-edges/DOID:9970.txt` | `2013-10-18 01:43:55.348572296 -0400` | 1104985
`131010-1/GaD-both/learning-edges/matched-negative-1s-1t.txt` | `2013-10-11 01:43:54.791641635 -0400` | 1330285
`131010-1/GaD-both/metaedge.txt` | `2013-10-18 01:43:41.240891641 -0400` | 42
`131010-1/GaD-both/metapaths/custom.txt` | `2013-10-11 01:52:56.622641320 -0400` | 8886
`131010-1/GaD-both/metapaths/length-2-cutoff.txt` | `2013-10-11 19:50:37.391437275 -0400` | 466
`131010-1/GaD-both/metapaths/length-3-cutoff.txt` | `2013-10-11 19:50:37.395437182 -0400` | 2351
`131010-1/GaD-both/metapaths/length-4-cutoff.txt` | `2013-10-11 19:50:37.403436996 -0400` | 10398
`131010-1/features/all-features.txt` | `2013-10-11 19:51:13.966585509 -0400` | 523
`131010-1/features/dwpc_0.5.txt` | `2013-09-04 17:23:32.725739000 -0400` | 77
`131010-1/graph/associated-gene-numbers.txt` | `2013-10-17 15:06:17.403130671 -0400` | 4168
`131010-1/graph/associated-gene-numbers.xlsx` | `2013-10-11 19:14:01.454586208 -0400` | 8884
`131010-1/graph/creation.log` | `2013-10-11 17:13:26.531657428 -0400` | 1797
`131010-1/graph/graph.json.gz` | `2013-10-11 17:16:51.622889958 -0400` | 7118250
`131010-1/graph/graph.pkl.gz` | `2013-10-11 17:15:05.681352593 -0400` | 22769095
`131014-1/GaD-both/analyses/learning-dwpc_0.5/arguments.cfg` | `2013-10-14 19:01:08.881082565 -0400` | 196
`131014-1/GaD-both/analyses/learning-dwpc_0.5/learning-features.txt` | `2013-10-15 06:09:35.125492573 -0400` | 3402510
`131014-1/GaD-both/analyses/multiple sclerosis/arguments.cfg` | `2013-10-14 19:01:49.508144142 -0400` | 192
`131014-1/GaD-both/analyses/multiple sclerosis/learning-features.txt` | `2013-10-15 09:37:44.126808488 -0400` | 8871761
`131014-1/GaD-both/learning-edges/matched-negative-1s-1t.txt` | `2013-10-14 18:49:58.644565064 -0400` | 1284122
`131014-1/GaD-both/learning-edges/multiple sclerosis.txt` | `2013-10-14 18:50:19.936073167 -0400` | 1104985
`131014-1/GaD-both/metaedge.txt` | `2013-10-14 18:49:56.500614595 -0400` | 42
`131014-1/GaD-both/metapaths/custom.txt` | `2013-10-11 01:52:56.622641000 -0400` | 8886
`131014-1/GaD-both/metapaths/length-2-cutoff.txt` | `2013-10-14 18:49:56.508614410 -0400` | 466
`131014-1/GaD-both/metapaths/length-3-cutoff.txt` | `2013-10-14 18:49:56.512614317 -0400` | 2351
`131014-1/GaD-both/metapaths/length-4-cutoff.txt` | `2013-10-14 18:49:56.540613671 -0400` | 10398
`131014-1/features/all-features.txt` | `2013-10-14 18:50:37.971656488 -0400` | 523
`131014-1/features/dwpc_0.5.txt` | `2013-09-04 17:23:32.725739000 -0400` | 77
`131014-1/graph/creation.log` | `2013-10-14 16:44:45.314247514 -0400` | 1837
`131014-1/graph/graph.json.gz` | `2013-10-14 16:48:43.352749371 -0400` | 7116954
`131014-1/graph/graph.pkl.gz` | `2013-10-14 16:46:41.743558424 -0400` | 22767612
`131014-1/graphics/MS-LD-novel-marginal-in-meta2.5.txt` | `2013-10-18 17:36:19.520138233 -0400` | 94169
`131014-1/graphics/multiple-sclerosis-prioritization.RData` | `2013-10-18 19:44:59.925715498 -0400` | 6336720
`131014-1/graphics/uncovering-wtccc2-roc.pdf` | `2013-10-18 17:35:52.004781504 -0400` | 37286
`131219-1/GaD-both/analyses/DOID:0050589/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 186
`131219-1/GaD-both/analyses/DOID:0050589/learning-features.txt` | `2013-12-20 02:31:56.000000000 -0500` | 7149394
`131219-1/GaD-both/analyses/DOID:10286/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10286/learning-features.txt` | `2013-12-20 01:06:16.000000000 -0500` | 6463724
`131219-1/GaD-both/analyses/DOID:1040/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1040/learning-features.txt` | `2013-12-20 01:11:16.000000000 -0500` | 5786501
`131219-1/GaD-both/analyses/DOID:10608/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10608/learning-features.txt` | `2013-12-20 01:56:54.000000000 -0500` | 6572110
`131219-1/GaD-both/analyses/DOID:10652/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10652/learning-features.txt` | `2013-12-20 01:16:54.000000000 -0500` | 6090716
`131219-1/GaD-both/analyses/DOID:10763/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10763/learning-features.txt` | `2013-12-20 01:16:44.000000000 -0500` | 6132437
`131219-1/GaD-both/analyses/DOID:10871/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10871/learning-features.txt` | `2013-12-20 01:12:38.000000000 -0500` | 5737132
`131219-1/GaD-both/analyses/DOID:10941/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:10941/learning-features.txt` | `2013-12-20 01:04:51.000000000 -0500` | 5536540
`131219-1/GaD-both/analyses/DOID:1107/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1107/learning-features.txt` | `2013-12-20 01:14:40.000000000 -0500` | 5918820
`131219-1/GaD-both/analyses/DOID:11830/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:11830/learning-features.txt` | `2013-12-20 00:54:59.000000000 -0500` | 5746420
`131219-1/GaD-both/analyses/DOID:12236/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:12236/learning-features.txt` | `2013-12-20 01:11:42.000000000 -0500` | 6146944
`131219-1/GaD-both/analyses/DOID:12306/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:12306/learning-features.txt` | `2013-12-20 01:45:38.000000000 -0500` | 6528456
`131219-1/GaD-both/analyses/DOID:12361/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:12361/learning-features.txt` | `2013-12-20 01:03:34.000000000 -0500` | 5467453
`131219-1/GaD-both/analyses/DOID:13241/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:13241/learning-features.txt` | `2013-12-20 01:06:56.000000000 -0500` | 5453380
`131219-1/GaD-both/analyses/DOID:13378/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:13378/learning-features.txt` | `2013-12-20 01:07:16.000000000 -0500` | 5403581
`131219-1/GaD-both/analyses/DOID:14221/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:14221/learning-features.txt` | `2013-12-20 01:32:55.000000000 -0500` | 6198166
`131219-1/GaD-both/analyses/DOID:14330/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 184
`131219-1/GaD-both/analyses/DOID:14330/learning-features.txt` | `2013-12-20 01:26:20.000000000 -0500` | 6200250
`131219-1/GaD-both/analyses/DOID:1459/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1459/learning-features.txt` | `2013-12-20 02:27:23.000000000 -0500` | 5717945
`131219-1/GaD-both/analyses/DOID:1595/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1595/learning-features.txt` | `2013-12-20 18:41:31.238090362 -0500` | 6543092
`131219-1/GaD-both/analyses/DOID:1826/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1826/learning-features.txt` | `2013-12-20 00:55:13.000000000 -0500` | 5600621
`131219-1/GaD-both/analyses/DOID:1909/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:1909/learning-features.txt` | `2013-12-20 01:20:01.000000000 -0500` | 5957424
`131219-1/GaD-both/analyses/DOID:216/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:216/learning-features.txt` | `2013-12-20 01:14:02.000000000 -0500` | 5991562
`131219-1/GaD-both/analyses/DOID:2377/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:2377/learning-features.txt` | `2013-12-20 01:42:46.000000000 -0500` | 6313130
`131219-1/GaD-both/analyses/DOID:2468/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:2468/learning-features.txt` | `2013-12-20 01:21:48.000000000 -0500` | 5930938
`131219-1/GaD-both/analyses/DOID:2841/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:2841/learning-features.txt` | `2013-12-20 01:25:49.000000000 -0500` | 6172183
`131219-1/GaD-both/analyses/DOID:2914/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:2914/learning-features.txt` | `2013-12-20 01:08:57.000000000 -0500` | 5875771
`131219-1/GaD-both/analyses/DOID:3083/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3083/learning-features.txt` | `2013-12-20 01:19:38.000000000 -0500` | 5840506
`131219-1/GaD-both/analyses/DOID:3310/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3310/learning-features.txt` | `2013-12-20 03:20:10.000000000 -0500` | 5977793
`131219-1/GaD-both/analyses/DOID:3312/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3312/learning-features.txt` | `2013-12-20 00:53:22.000000000 -0500` | 5576334
`131219-1/GaD-both/analyses/DOID:332/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:332/learning-features.txt` | `2013-12-20 00:57:30.000000000 -0500` | 5649916
`131219-1/GaD-both/analyses/DOID:3393/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3393/learning-features.txt` | `2013-12-20 01:41:22.000000000 -0500` | 6426448
`131219-1/GaD-both/analyses/DOID:3459/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3459/learning-features.txt` | `2013-12-20 01:41:31.000000000 -0500` | 6320889
`131219-1/GaD-both/analyses/DOID:3905/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:3905/learning-features.txt` | `2013-12-20 01:12:17.000000000 -0500` | 5771582
`131219-1/GaD-both/analyses/DOID:4007/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:4007/learning-features.txt` | `2013-12-20 01:17:58.000000000 -0500` | 5855071
`131219-1/GaD-both/analyses/DOID:4905/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:4905/learning-features.txt` | `2013-12-20 01:06:17.000000000 -0500` | 5698008
`131219-1/GaD-both/analyses/DOID:5419/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:5419/learning-features.txt` | `2013-12-20 01:34:08.000000000 -0500` | 6021884
`131219-1/GaD-both/analyses/DOID:557/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:557/learning-features.txt` | `2013-12-20 00:53:10.000000000 -0500` | 5627821
`131219-1/GaD-both/analyses/DOID:5844/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:5844/learning-features.txt` | `2013-12-20 00:57:35.000000000 -0500` | 5795099
`131219-1/GaD-both/analyses/DOID:635/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:635/learning-features.txt` | `2013-12-20 01:01:00.000000000 -0500` | 5171661
`131219-1/GaD-both/analyses/DOID:6364/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:6364/learning-features.txt` | `2013-12-20 01:08:41.000000000 -0500` | 5521114
`131219-1/GaD-both/analyses/DOID:678/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:678/learning-features.txt` | `2013-12-20 01:08:49.000000000 -0500` | 5603961
`131219-1/GaD-both/analyses/DOID:7147/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:7147/learning-features.txt` | `2013-12-20 01:06:59.000000000 -0500` | 5591423
`131219-1/GaD-both/analyses/DOID:7148/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:7148/learning-features.txt` | `2013-12-20 01:20:20.000000000 -0500` | 6270900
`131219-1/GaD-both/analyses/DOID:824/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:824/learning-features.txt` | `2013-12-20 03:02:13.000000000 -0500` | 6065500
`131219-1/GaD-both/analyses/DOID:8577/arguments.cfg` | `2013-12-20 00:38:33.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:8577/learning-features.txt` | `2013-12-20 02:13:00.000000000 -0500` | 6839842
`131219-1/GaD-both/analyses/DOID:8778/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:8778/learning-features.txt` | `2013-12-20 04:07:45.000000000 -0500` | 7054799
`131219-1/GaD-both/analyses/DOID:8893/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:8893/learning-features.txt` | `2013-12-20 01:11:12.000000000 -0500` | 5942154
`131219-1/GaD-both/analyses/DOID:9074/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:9074/learning-features.txt` | `2013-12-20 01:39:24.000000000 -0500` | 6352842
`131219-1/GaD-both/analyses/DOID:9352/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:9352/learning-features.txt` | `2013-12-20 01:36:41.000000000 -0500` | 6657359
`131219-1/GaD-both/analyses/DOID:9744/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:9744/learning-features.txt` | `2013-12-20 01:48:45.000000000 -0500` | 6622159
`131219-1/GaD-both/analyses/DOID:986/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 182
`131219-1/GaD-both/analyses/DOID:986/learning-features.txt` | `2013-12-20 01:07:02.000000000 -0500` | 5356362
`131219-1/GaD-both/analyses/DOID:9970/arguments.cfg` | `2013-12-20 00:38:32.000000000 -0500` | 183
`131219-1/GaD-both/analyses/DOID:9970/learning-features.txt` | `2013-12-20 01:18:14.000000000 -0500` | 6175527
`131219-1/GaD-both/graphics/auc-scatterplot.pdf` | `2014-01-29 16:17:37.665458138 -0500` | 5402
`131219-1/GaD-both/graphics/disease-performance-train-on-all.txt` | `2014-01-29 16:04:23.474090207 -0500` | 3207
`131219-1/GaD-both/graphics/metapath-addition.txt` | `2013-12-20 18:44:40.366207776 -0500` | 28342
`131219-1/GaD-both/graphics/metapath-performance-msig.pdf` | `2014-01-29 12:52:05.057520230 -0500` | 61447
`131219-1/GaD-both/graphics/metapath-performance-nonmsig.pdf` | `2013-12-20 18:46:58.367366814 -0500` | 37703
`131219-1/GaD-both/graphics/metapath-performance.pdf` | `2013-12-20 18:46:53.651464006 -0500` | 93967
`131219-1/GaD-both/graphics/model-performance-2.pdf` | `2014-01-29 16:04:39.025764981 -0500` | 7744
`131219-1/GaD-both/learning-edges/DOID:0050589.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1162249
`131219-1/GaD-both/learning-edges/DOID:10286.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:1040.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:10608.txt` | `2013-12-20 00:38:27.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:10652.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:10763.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:10871.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:10941.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:1107.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:11830.txt` | `2013-12-20 00:38:27.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:12236.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:12306.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:12361.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:13241.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:13378.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:14221.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:14330.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1124073
`131219-1/GaD-both/learning-edges/DOID:1459.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:1595.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:1826.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:1909.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:216.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:2377.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:2468.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:2841.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:2914.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:3083.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:3310.txt` | `2013-12-20 00:38:32.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:3312.txt` | `2013-12-20 00:38:32.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:332.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:3393.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:3459.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:3905.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:4007.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:4905.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:5419.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:557.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:5844.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:635.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:6364.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:678.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:7147.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:7148.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:824.txt` | `2013-12-20 00:38:27.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:8577.txt` | `2013-12-20 00:38:31.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:8778.txt` | `2013-12-20 00:38:30.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:8893.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:9074.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:9352.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:9744.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1104985
`131219-1/GaD-both/learning-edges/DOID:986.txt` | `2013-12-20 00:38:29.000000000 -0500` | 1085897
`131219-1/GaD-both/learning-edges/DOID:9970.txt` | `2013-12-20 00:38:28.000000000 -0500` | 1104985
`131219-1/GaD-both/metaedge.txt` | `2013-12-20 00:38:27.000000000 -0500` | 42
`131219-1/GaD-both/metapaths/custom.txt` | `2013-12-20 00:38:27.000000000 -0500` | 4344
`131219-1/GaD-both/metapaths/length-2-cutoff.txt` | `2013-12-20 00:38:27.000000000 -0500` | 372
`131219-1/GaD-both/metapaths/length-3-cutoff.txt` | `2013-12-20 00:38:27.000000000 -0500` | 4396
`131219-1/features/all-features.txt` | `2013-12-20 00:38:33.000000000 -0500` | 523
`131219-1/features/dwpc_0.5.txt` | `2013-12-20 00:38:33.000000000 -0500` | 77
`131219-1/graph/creation.log` | `2013-12-20 00:38:19.000000000 -0500` | 2069
`131219-1/graph/graph.json.gz` | `2013-12-20 00:38:27.000000000 -0500` | 43822913
`131219-1/graph/graph.pkl.gz` | `2013-12-20 00:38:24.000000000 -0500` | 75264723
`140302-parsweep/feature-aucs.txt` | `2014-03-12 18:25:05.170492000 -0400` | 61823
`140302-parsweep/graph/creation.log` | `2014-03-04 16:22:58.068997039 -0500` | 3465
`140302-parsweep/graph/graph.json.gz` | `2014-03-04 16:30:37.866478174 -0500` | 17821593
`140302-parsweep/graph/graph.pkl.gz` | `2014-03-04 16:26:26.176238653 -0500` | 44873861
`140302-parsweep/negatives-0.5-percent/AUC-lineplots.pdf` | `2014-03-10 22:09:50.949643442 -0400` | 7968
`140302-parsweep/negatives-0.5-percent/AUC-tileplot.pdf` | `2014-03-10 22:09:51.297635538 -0400` | 8448
`140302-parsweep/negatives-0.5-percent/feature-aucs.txt` | `2014-03-10 22:04:36.056794984 -0400` | 61793
`140302-parsweep/negatives-0.5-percent/features-subset-fine.txt.gz` | `2014-03-11 00:26:31.548831297 -0400` | 2240786
`140302-parsweep/negatives-2.0-percent/AUC-lineplots.pdf` | `2014-03-12 18:35:16.372170456 -0400` | 8617
`140302-parsweep/negatives-2.0-percent/AUC-tileplot.pdf` | `2014-03-12 18:35:16.768161181 -0400` | 8423
`140302-parsweep/negatives-2.0-percent/feature-aucs.txt` | `2014-03-12 18:25:05.170492991 -0400` | 61823
`140302-parsweep/negatives-2.0-percent/features-subset-fine-2percent.txt.gz` | `2014-03-11 06:32:33.597543222 -0400` | 4182414
`140305-algosweep/features-edge-exclusions.txt.gz` | `2014-03-13 15:29:55.772833009 -0400` | 22448944
`140305-algosweep/features-part.txt.gz` | `2014-03-06 17:42:10.393146000 -0500` | 20182890
`140305-algosweep/features.txt.gz` | `2014-03-15 03:01:18.056181380 -0400` | 197389438
`140305-algosweep/graph/creation.log` | `2014-03-05 19:08:51.495182873 -0500` | 1423
`140305-algosweep/graph/graph.json.gz` | `2014-03-05 19:21:47.653253492 -0500` | 42176983
`140305-algosweep/graph/graph.pkl.gz` | `2014-03-05 19:15:11.354412285 -0500` | 70921828
`140310-parsweep/AUC-lineplots.pdf` | `2014-03-17 17:19:17.089917544 -0400` | 9859
`140310-parsweep/AUC-tileplot.pdf` | `2014-03-17 17:19:17.349911109 -0400` | 13330
`140310-parsweep/feature-aucs.txt` | `2014-03-17 13:05:06.672378391 -0400` | 150988
`140310-parsweep/features.txt.gz` | `2014-03-15 21:09:34.369726745 -0400` | 13241357
`140310-parsweep/graph/creation.log` | `2014-03-10 22:28:31.064207623 -0400` | 3466
`140310-parsweep/graph/graph.json.gz` | `2014-03-10 22:40:40.055661458 -0400` | 28817042
`140310-parsweep/graph/graph.pkl.gz` | `2014-03-10 22:33:52.208919746 -0400` | 69956081
`code/bayesian.R` | `2013-10-16 15:15:40.316664689 -0400` | 341
`code/disease-specific-performance.R` | `2013-10-21 12:48:18.124039906 -0400` | 1796
`code/feature-parser.R` | `2013-10-17 13:30:07.172299383 -0400` | 3250
`code/full-modeler.R` | `2014-01-29 16:17:33.293549802 -0500` | 5577
`code/future-ms-performance.R` | `2013-10-21 13:04:33.997191970 -0400` | 5431
`code/metaedge-comparison-modeless.R` | `2014-01-29 12:51:29.470288596 -0500` | 5357
`code/metaedge-comparison.R` | `2013-10-21 17:42:23.554958145 -0400` | 5824
`code/method-comparison.R` | `2013-10-16 21:01:36.995416071 -0400` | 2899
`code/outdated/analysis-toolkit.R` | `2013-10-11 20:20:51.336890916 -0400` | 5785
`code/outdated/analysis_pipeline.R` | `2013-07-19 14:32:12.815254403 -0400` | 13604
`code/outdated/code.R` | `2013-07-17 20:13:00.077718741 -0400` | 5905
`code/outdated/gwas-bayesian-integration.R` | `2013-10-16 17:33:42.464517812 -0400` | 7437
`code/outdated/metaedge-comparison-old.R` | `2013-10-17 13:37:27.089955205 -0400` | 3105
`code/performance.R` | `2014-01-28 23:53:59.894950131 -0500` | 1287
`copub/doid-bto-cooccurrences-bodymap2-tissues.txt` | `2013-10-11 00:44:48.216349184 -0400` | 125031
`copub/doid-bto-cooccurrences-gnf-tissues.txt` | `2014-02-05 17:29:12.786126567 -0500` | 513284
`copub/tiger-tissue-to-disease-cooccurrences.txt` | `2013-07-11 18:16:32.733754999 -0400` | 61016
`diseases-DE-gene-numbers.txt` | `2013-07-11 18:16:32.709754999 -0400` | 2201
`gwas/exploratory.py` | `2013-07-11 18:17:52.133754999 -0400` | 1532
`gwas/meta2.5-genewise.txt` | `2013-07-11 18:17:52.181754999 -0400` | 1668351
`gwas/p-value-histograms.pdf` | `2013-07-11 18:17:52.181754999 -0400` | 5547
`gwas/wtccc2-genewise.txt` | `2013-07-11 18:17:52.257754999 -0400` | 1692273
`imp-optimizer/imp-optimizer-anal.sh` | `2013-08-01 15:15:22.286642522 -0400` | 1427
`imp-optimizer/imp-optimizer.sh` | `2013-08-01 14:33:56.374670602 -0400` | 1136
`imp-optimizer/nohup.out` | `2013-08-01 15:12:35.734644403 -0400` | 930
`imp-optimizer/positives/features/learning-features.txt` | `2013-08-02 22:51:48.053357205 -0400` | 1741067
`imp-optimizer/positives/network_creation.log` | `2013-08-01 14:52:46.626657835 -0400` | 752
`imp-optimizer/positives/prepared-graph.pkl` | `2013-08-01 15:11:22.190645234 -0400` | 37711557
`imp-optimizer/positives/raw-graph.gml` | `2013-08-01 14:53:09.098657581 -0400` | 30702918
`imp-optimizer/positives/raw-graph.pkl` | `2013-08-01 14:53:18.030657480 -0400` | 36633253
`imp-optimizer/positives_and_predictions_0.1/features/learning-features.txt` | `2013-08-01 15:09:52.934646242 -0400` | 1141
`imp-optimizer/positives_and_predictions_0.1/network_creation.log` | `2013-08-01 14:53:39.442657238 -0400` | 753
`imp-optimizer/positives_and_predictions_0.1/prepared-graph.pkl` | `2013-08-01 15:09:07.902646751 -0400` | 96182093
`imp-optimizer/positives_and_predictions_0.1/raw-graph.gml` | `2013-08-01 14:54:40.954656543 -0400` | 78822643
`imp-optimizer/positives_and_predictions_0.1/raw-graph.pkl` | `2013-08-01 14:55:02.538656300 -0400` | 94322523
`imp-optimizer/positives_and_predictions_0.2/network_creation.log` | `2013-08-01 14:55:17.050656136 -0400` | 753
`imp-optimizer/positives_and_predictions_0.2/raw-graph.gml` | `2013-08-01 14:55:45.890655810 -0400` | 37543253
`imp-optimizer/positives_and_predictions_0.2/raw-graph.pkl` | `2013-08-01 14:55:52.982655730 -0400` | 44379486
`imp-optimizer/positives_and_predictions_0.3/features/learning-features.txt` | `2013-08-02 23:56:00.077313693 -0400` | 1813374
`imp-optimizer/positives_and_predictions_0.3/network_creation.log` | `2013-08-01 14:56:04.238655603 -0400` | 752
`imp-optimizer/positives_and_predictions_0.3/prepared-graph.pkl` | `2013-08-01 15:14:44.258642951 -0400` | 40835622
`imp-optimizer/positives_and_predictions_0.3/raw-graph.gml` | `2013-08-01 14:56:28.222655332 -0400` | 33602054
`imp-optimizer/positives_and_predictions_0.3/raw-graph.pkl` | `2013-08-01 14:56:35.866655245 -0400` | 39694204
`imp-optimizer/positives_and_predictions_0.4/features/learning-features.txt` | `2013-08-02 23:37:10.589326452 -0400` | 1785598
`imp-optimizer/positives_and_predictions_0.4/network_creation.log` | `2013-08-01 14:56:47.058655119 -0400` | 752
`imp-optimizer/positives_and_predictions_0.4/prepared-graph.pkl` | `2013-08-01 15:15:25.974642480 -0400` | 39373627
`imp-optimizer/positives_and_predictions_0.4/raw-graph.gml` | `2013-08-01 14:57:10.742654852 -0400` | 32346094
`imp-optimizer/positives_and_predictions_0.4/raw-graph.pkl` | `2013-08-01 14:57:18.158654768 -0400` | 38256337
`imp-optimizer/positives_and_predictions_0.5/features/learning-features.txt` | `2013-08-02 23:22:41.877336265 -0400` | 1765848
`imp-optimizer/positives_and_predictions_0.5/network_creation.log` | `2013-08-01 14:57:29.198654643 -0400` | 752
`imp-optimizer/positives_and_predictions_0.5/prepared-graph.pkl` | `2013-08-01 15:15:34.966642378 -0400` | 38649810
`imp-optimizer/positives_and_predictions_0.5/raw-graph.gml` | `2013-08-01 14:57:54.310654359 -0400` | 31669768
`imp-optimizer/positives_and_predictions_0.5/raw-graph.pkl` | `2013-08-01 14:58:02.850654263 -0400` | 37548103
`imp-optimizer/positives_and_predictions_0.6/features/learning-features.txt` | `2013-08-02 23:22:05.889336671 -0400` | 1748540
`imp-optimizer/positives_and_predictions_0.6/network_creation.log` | `2013-08-01 14:58:13.774654139 -0400` | 752
`imp-optimizer/positives_and_predictions_0.6/prepared-graph.pkl` | `2013-08-01 15:15:57.590642123 -0400` | 38081007
`imp-optimizer/positives_and_predictions_0.6/raw-graph.gml` | `2013-08-01 14:58:37.318653874 -0400` | 31094458
`imp-optimizer/positives_and_predictions_0.6/raw-graph.pkl` | `2013-08-01 14:58:46.130653774 -0400` | 36994043
`imp-optimizer/positives_and_predictions_0.7/features/learning-features.txt` | `2013-08-02 23:13:59.881342161 -0400` | 1741048
`imp-optimizer/positives_and_predictions_0.7/network_creation.log` | `2013-08-01 14:58:57.958653640 -0400` | 752
`imp-optimizer/positives_and_predictions_0.7/prepared-graph.pkl` | `2013-08-01 15:16:14.438641932 -0400` | 37713527
`imp-optimizer/positives_and_predictions_0.7/raw-graph.gml` | `2013-08-01 14:59:21.494653375 -0400` | 30705115
`imp-optimizer/positives_and_predictions_0.7/raw-graph.pkl` | `2013-08-01 14:59:29.670653282 -0400` | 36635161
`imp-optimizer/positives_and_predictions_0.8/features/learning-features.txt` | `2013-08-02 23:10:28.761344546 -0400` | 1741067
`imp-optimizer/positives_and_predictions_0.8/network_creation.log` | `2013-08-01 14:59:40.914653155 -0400` | 752
`imp-optimizer/positives_and_predictions_0.8/prepared-graph.pkl` | `2013-08-01 15:16:28.694641771 -0400` | 37711577
`imp-optimizer/positives_and_predictions_0.8/raw-graph.gml` | `2013-08-01 15:00:03.638652898 -0400` | 30702918
`imp-optimizer/positives_and_predictions_0.8/raw-graph.pkl` | `2013-08-01 15:00:11.062652815 -0400` | 36633273
`imp-optimizer/positives_and_predictions_0.9/network_creation.log` | `2013-08-01 15:00:22.054652690 -0400` | 752
`imp-optimizer/positives_and_predictions_0.9/raw-graph.gml` | `2013-08-01 15:00:45.254652428 -0400` | 30702918
`imp-optimizer/positives_and_predictions_0.9/raw-graph.pkl` | `2013-08-01 15:00:52.870652342 -0400` | 36633273
`imp-optimizer/predictions_0.1/network_creation.log` | `2013-08-01 15:01:08.158652170 -0400` | 753
`imp-optimizer/predictions_0.1/raw-graph.gml` | `2013-08-01 15:01:50.990651686 -0400` | 52019331
`imp-optimizer/predictions_0.1/raw-graph.pkl` | `2013-08-01 15:02:04.430651534 -0400` | 62219851
`imp-optimizer/predictions_0.2/network_creation.log` | `2013-08-01 15:02:11.506651454 -0400` | 752
`imp-optimizer/predictions_0.2/raw-graph.gml` | `2013-08-01 15:02:18.350651377 -0400` | 9602297
`imp-optimizer/predictions_0.2/raw-graph.pkl` | `2013-08-01 15:02:20.022651358 -0400` | 10881172
`imp-optimizer/predictions_0.3/features/learning-features.txt` | `2013-08-01 15:18:53.686640134 -0400` | 980860
`imp-optimizer/predictions_0.3/network_creation.log` | `2013-08-01 15:02:24.362651309 -0400` | 752
`imp-optimizer/predictions_0.3/prepared-graph.pkl` | `2013-08-01 15:16:15.350641922 -0400` | 6085767
`imp-optimizer/predictions_0.3/raw-graph.gml` | `2013-08-01 15:02:27.714651271 -0400` | 5053192
`imp-optimizer/predictions_0.3/raw-graph.pkl` | `2013-08-01 15:02:28.278651265 -0400` | 5463818
`imp-optimizer/predictions_0.4/features/learning-features.txt` | `2013-08-01 15:17:25.574641129 -0400` | 869538
`imp-optimizer/predictions_0.4/network_creation.log` | `2013-08-01 15:02:32.194651220 -0400` | 751
`imp-optimizer/predictions_0.4/prepared-graph.pkl` | `2013-08-01 15:16:31.486641740 -0400` | 3970931
`imp-optimizer/predictions_0.4/raw-graph.gml` | `2013-08-01 15:02:34.130651199 -0400` | 3264027
`imp-optimizer/predictions_0.4/raw-graph.pkl` | `2013-08-01 15:02:34.454651195 -0400` | 3392225
`imp-optimizer/predictions_0.5/network_creation.log` | `2013-08-01 15:02:38.234651152 -0400` | 750
`imp-optimizer/predictions_0.5/raw-graph.gml` | `2013-08-01 15:02:39.338651140 -0400` | 2060055
`imp-optimizer/predictions_0.5/raw-graph.pkl` | `2013-08-01 15:02:39.854651134 -0400` | 2078814
`imp-optimizer/predictions_0.6/network_creation.log` | `2013-08-01 15:02:43.246651096 -0400` | 749
`imp-optimizer/predictions_0.6/raw-graph.gml` | `2013-08-01 15:02:43.890651088 -0400` | 991524
`imp-optimizer/predictions_0.6/raw-graph.pkl` | `2013-08-01 15:02:43.974651087 -0400` | 964850
`imp-optimizer/predictions_0.7/network_creation.log` | `2013-08-01 15:02:47.198651051 -0400` | 748
`imp-optimizer/predictions_0.7/raw-graph.gml` | `2013-08-01 15:02:47.334651049 -0400` | 344089
`imp-optimizer/predictions_0.7/raw-graph.pkl` | `2013-08-01 15:02:47.358651049 -0400` | 315907
`imp-optimizer/predictions_0.8/network_creation.log` | `2013-08-01 15:02:50.466651014 -0400` | 748
`imp-optimizer/predictions_0.8/raw-graph.gml` | `2013-08-01 15:02:50.590651013 -0400` | 308631
`imp-optimizer/predictions_0.8/raw-graph.pkl` | `2013-08-01 15:02:50.614651012 -0400` | 282679
`imp-optimizer/predictions_0.9/network_creation.log` | `2013-08-01 15:02:53.690650978 -0400` | 747
`imp-optimizer/predictions_0.9/raw-graph.gml` | `2013-08-01 15:02:53.810650976 -0400` | 292718
`imp-optimizer/predictions_0.9/raw-graph.pkl` | `2013-08-01 15:02:53.830650976 -0400` | 268102
`mappings/bto-to-copub-mappings-bodymap2-manual.txt` | `2013-10-10 23:50:02.198461047 -0400` | 629
`mappings/bto-to-copub-mappings-gnf-auto.txt` | `2014-02-05 14:45:46.417330151 -0500` | 3172
`mappings/bto-to-copub-mappings-gnf-manual.txt` | `2014-02-05 16:18:40.024026730 -0500` | 3031
`mappings/doid-to-copub-mappings-auto.txt` | `2013-10-10 22:12:39.236361426 -0400` | 7490
`mappings/doid-to-copub-mappings-manual.txt` | `2013-10-10 23:40:13.727588986 -0400` | 7475
`mappings/efo-copub-etiome-manual.txt` | `2013-07-26 20:04:57.718421000 -0400` | 3438
`mappings/efo-to-copub-mappings-auto.txt` | `2013-07-11 18:16:32.709754999 -0400` | 3055
`mappings/efo-to-copub-mappings-manual.txt` | `2013-07-11 18:16:32.709754999 -0400` | 2609
`mappings/tiger-tissue-to-copub-mappings-auto.txt` | `2013-07-11 18:16:32.709754999 -0400` | 778
`mappings/tiger-tissue-to-copub-mappings-manual.txt` | `2013-07-11 18:16:32.709754999 -0400` | 781

## Poster files

Table created with shell command:

```bash
echo "file_name | last_modified | size_bytes
--- | --- | ---
$(git diff-tree --no-commit-id --name-only -r b7701f11 | xargs --delimiter='\n' stat --format='`%n` | `%y` | %s')
" >> readme-timestamps.md
```

file_name | last_modified | size_bytes
--- | --- | ---
`poster/MS-LD-novel-marginal-in-meta2.5.txt` | `2013-10-18 17:36:19.000000000 -0400` | 94169
`poster/MS-gwas-histograms.pdf` | `2013-07-11 18:17:52.000000000 -0400` | 5547
`poster/UC logo/Bitmap/High Res/CMYK/uc_seal_blue.tif` | `2013-03-05 19:30:17.000000000 -0500` | 3270150
`poster/UC logo/Bitmap/High Res/CMYK/uc_seal_lock-up_blue.tif` | `2013-03-05 19:32:39.000000000 -0500` | 9746716
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_block_fill_blue.tif` | `2013-03-05 19:33:24.000000000 -0500` | 6056670
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_block_stroke_blue.tif` | `2013-03-05 19:35:12.000000000 -0500` | 6059574
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_single_blue.tif` | `2013-03-05 19:36:20.000000000 -0500` | 1620834
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_single_gold.tif` | `2011-11-30 20:46:31.000000000 -0500` | 1619834
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_single_gray.tif` | `2012-02-06 17:29:58.000000000 -0500` | 1621326
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_stacked_blue.tif` | `2013-03-05 19:51:24.000000000 -0500` | 5102068
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_stacked_gold.tif` | `2011-11-30 20:43:53.000000000 -0500` | 5097736
`poster/UC logo/Bitmap/High Res/CMYK/uc_wordmark_stacked_gray.tif` | `2012-02-06 17:28:34.000000000 -0500` | 5097424
`poster/UC logo/Bitmap/High Res/Grayscale/uc_seal_black.tif` | `2011-07-13 18:06:38.000000000 -0400` | 854174
`poster/UC logo/Bitmap/High Res/Grayscale/uc_seal_lock-up_black.tif` | `2011-07-13 18:08:16.000000000 -0400` | 2452278
`poster/UC logo/Bitmap/High Res/Grayscale/uc_wordmark_block_fill_black.tif` | `2013-03-04 19:09:36.000000000 -0500` | 6050398
`poster/UC logo/Bitmap/High Res/Grayscale/uc_wordmark_block_stroke_black.tif` | `2013-03-04 19:10:25.000000000 -0500` | 6052080
`poster/UC logo/Bitmap/High Res/Grayscale/uc_wordmark_single_black.tif` | `2011-07-13 17:59:11.000000000 -0400` | 416082
`poster/UC logo/Bitmap/High Res/Grayscale/uc_wordmark_stacked_black.tif` | `2011-07-13 17:52:02.000000000 -0400` | 1290340
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_seal_black.png` | `2011-07-13 18:16:17.000000000 -0400` | 17256
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_seal_lock-up_black.png` | `2011-07-13 18:16:37.000000000 -0400` | 27570
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_seal_lock-up_white.png` | `2011-07-13 18:16:28.000000000 -0400` | 27796
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_seal_white.png` | `2011-07-13 18:16:05.000000000 -0400` | 16585
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_block_fill_black.png` | `2013-03-04 19:12:26.000000000 -0500` | 9723
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_block_fill_transparent_black.png` | `2013-03-04 19:14:04.000000000 -0500` | 7709
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_block_fill_transparent_white.png` | `2013-03-04 19:14:50.000000000 -0500` | 7120
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_block_stroke_black.png` | `2013-03-04 19:15:37.000000000 -0500` | 7796
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_block_stroke_white.png` | `2013-03-04 19:16:01.000000000 -0500` | 7181
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_single_black.png` | `2011-07-13 18:13:15.000000000 -0400` | 4598
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_single_white.png` | `2011-07-13 18:13:02.000000000 -0400` | 4263
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_stacked_black.png` | `2011-07-13 18:12:45.000000000 -0400` | 7314
`poster/UC logo/Bitmap/Low Res/Grayscale/uc_wordmark_stacked_white.png` | `2011-07-13 18:12:28.000000000 -0400` | 6821
`poster/UC logo/Bitmap/Low Res/RGB/uc_seal_blue.png` | `2013-03-05 19:55:16.000000000 -0500` | 21237
`poster/UC logo/Bitmap/Low Res/RGB/uc_seal_lock-up_blue.png` | `2013-03-05 19:55:38.000000000 -0500` | 31771
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_block_fill_blue.png` | `2013-03-05 19:57:20.000000000 -0500` | 8727
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_block_fill_transparent_blue.png` | `2013-03-05 20:00:04.000000000 -0500` | 7769
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_block_stroke_blue.png` | `2013-03-05 20:01:36.000000000 -0500` | 7896
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_single_blue.png` | `2013-03-05 20:02:19.000000000 -0500` | 4695
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_single_gold.png` | `2011-07-13 18:18:40.000000000 -0400` | 4714
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_single_gray.png` | `2012-02-06 17:31:15.000000000 -0500` | 7187
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_stacked_blue.png` | `2013-03-05 20:04:57.000000000 -0500` | 7371
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_stacked_gold.png` | `2011-07-13 18:18:20.000000000 -0400` | 7425
`poster/UC logo/Bitmap/Low Res/RGB/uc_wordmark_stacked_gray.png` | `2012-02-06 17:32:10.000000000 -0500` | 11540
`poster/UC logo/Vector/CMYK/uc_seal_blue_cmyk.eps` | `2013-03-05 18:09:15.000000000 -0500` | 839718
`poster/UC logo/Vector/CMYK/uc_seal_lock-up_blue_cmyk.eps` | `2013-03-05 19:31:59.000000000 -0500` | 987966
`poster/UC logo/Vector/CMYK/uc_wordmark_block_fill_blue_cmyk.eps` | `2013-03-05 18:15:59.000000000 -0500` | 594830
`poster/UC logo/Vector/CMYK/uc_wordmark_block_fill_transparent_blue_cmyk.eps` | `2013-03-05 18:20:05.000000000 -0500` | 610290
`poster/UC logo/Vector/CMYK/uc_wordmark_block_stroke_blue_cmyk.eps` | `2013-03-05 18:19:33.000000000 -0500` | 611514
`poster/UC logo/Vector/CMYK/uc_wordmark_single_blue_cmyk.eps` | `2013-03-05 18:19:07.000000000 -0500` | 456694
`poster/UC logo/Vector/CMYK/uc_wordmark_single_gold_cmyk.eps` | `2012-02-06 17:49:27.000000000 -0500` | 456022
`poster/UC logo/Vector/CMYK/uc_wordmark_single_gray_cmyk.eps` | `2012-02-06 17:26:40.000000000 -0500` | 458178
`poster/UC logo/Vector/CMYK/uc_wordmark_stacked_blue_cmyk.eps` | `2013-03-05 18:18:34.000000000 -0500` | 574770
`poster/UC logo/Vector/CMYK/uc_wordmark_stacked_gold_cmyk.eps` | `2012-02-06 17:48:30.000000000 -0500` | 570798
`poster/UC logo/Vector/CMYK/uc_wordmark_stacked_gray_cmyk.eps` | `2012-02-06 17:27:00.000000000 -0500` | 571826
`poster/UC logo/Vector/Grayscale/uc_seal_black.eps` | `2013-03-04 18:25:42.000000000 -0500` | 820574
`poster/UC logo/Vector/Grayscale/uc_seal_lock-up_black.eps` | `2013-03-04 18:25:21.000000000 -0500` | 981346
`poster/UC logo/Vector/Grayscale/uc_seal_lock-up_white.eps` | `2013-03-04 18:25:12.000000000 -0500` | 961230
`poster/UC logo/Vector/Grayscale/uc_seal_white.eps` | `2012-02-06 17:46:41.000000000 -0500` | 751182
`poster/UC logo/Vector/Grayscale/uc_wordmark_block_fill_black.eps` | `2013-03-04 18:55:05.000000000 -0500` | 590882
`poster/UC logo/Vector/Grayscale/uc_wordmark_block_fill_transparent_black.eps` | `2013-03-04 18:27:49.000000000 -0500` | 606638
`poster/UC logo/Vector/Grayscale/uc_wordmark_block_fill_transparent_white.eps` | `2013-03-04 18:28:19.000000000 -0500` | 580402
`poster/UC logo/Vector/Grayscale/uc_wordmark_block_stroke_black.eps` | `2013-03-04 18:22:34.000000000 -0500` | 603782
`poster/UC logo/Vector/Grayscale/uc_wordmark_block_stroke_white.eps` | `2013-03-04 18:24:20.000000000 -0500` | 580466
`poster/UC logo/Vector/Grayscale/uc_wordmark_single_black.eps` | `2013-03-04 18:24:49.000000000 -0500` | 454278
`poster/UC logo/Vector/Grayscale/uc_wordmark_single_white.eps` | `2012-02-06 17:46:20.000000000 -0500` | 448814
`poster/UC logo/Vector/Grayscale/uc_wordmark_stacked_black.eps` | `2013-03-04 18:24:38.000000000 -0500` | 568414
`poster/UC logo/Vector/Grayscale/uc_wordmark_stacked_white.eps` | `2012-02-06 17:45:47.000000000 -0500` | 549066
`poster/UC logo/Vector/Spot/uc_seal_blue_spot.eps` | `2013-03-05 18:52:33.000000000 -0500` | 840850
`poster/UC logo/Vector/Spot/uc_seal_lock-up_blue_spot.eps` | `2013-03-05 19:24:50.000000000 -0500` | 988650
`poster/UC logo/Vector/Spot/uc_wordmark_block_fill_blue_spot.eps` | `2013-03-05 19:23:10.000000000 -0500` | 595690
`poster/UC logo/Vector/Spot/uc_wordmark_block_fill_transparent_blue_spot.eps` | `2013-03-05 19:13:06.000000000 -0500` | 611414
`poster/UC logo/Vector/Spot/uc_wordmark_block_stroke_blue_spot.eps` | `2013-03-05 19:03:01.000000000 -0500` | 613022
`poster/UC logo/Vector/Spot/uc_wordmark_single_blue_spot.eps` | `2013-03-05 18:30:02.000000000 -0500` | 457538
`poster/UC logo/Vector/Spot/uc_wordmark_single_gold_spot.eps` | `2012-02-06 17:39:45.000000000 -0500` | 456618
`poster/UC logo/Vector/Spot/uc_wordmark_single_gray_spot.eps` | `2012-02-06 17:15:15.000000000 -0500` | 456730
`poster/UC logo/Vector/Spot/uc_wordmark_stacked_blue_spot.eps` | `2013-03-05 18:22:37.000000000 -0500` | 575610
`poster/UC logo/Vector/Spot/uc_wordmark_stacked_gold_spot.eps` | `2012-02-06 17:38:42.000000000 -0500` | 571658
`poster/UC logo/Vector/Spot/uc_wordmark_stacked_gray_spot.eps` | `2012-02-06 17:14:24.000000000 -0500` | 569570
`poster/UC logo/ucsf_tag_sig_2C_OTL_9.eps` | `2007-04-12 12:29:28.000000000 -0400` | 224891
`poster/disease-specific-performance.txt` | `2013-10-21 13:37:12.000000000 -0400` | 2678
`poster/graph-IRF1.py` | `2013-10-18 20:18:14.000000000 -0400` | 703
`poster/metaedge-exclusion-performance.pdf` | `2013-10-21 17:42:35.000000000 -0400` | 22076
`poster/metapath-performance-including-full.pdf` | `2013-10-21 13:43:25.000000000 -0400` | 22124
`poster/metapath-performance.pdf` | `2013-10-21 13:38:31.000000000 -0400` | 19244
`poster/poster-tables.numbers/Data/droppedImage-15.pdf` | `2014-01-08 21:21:43.000000000 -0500` | 70021
`poster/poster-tables.numbers/Data/droppedImage-17.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 47482
`poster/poster-tables.numbers/Data/droppedImage-19.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 31975
`poster/poster-tables.numbers/Data/droppedImage-21.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 9165
`poster/poster-tables.numbers/Data/droppedImage-22.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 8935
`poster/poster-tables.numbers/Data/droppedImage-23.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 25081
`poster/poster-tables.numbers/Data/droppedImage-24.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 24828
`poster/poster-tables.numbers/Data/droppedImage-25.pdf` | `2014-01-08 21:21:44.000000000 -0500` | 25232
`poster/poster-tables.numbers/Data/droppedImage-small-16.png` | `2014-01-08 21:21:44.000000000 -0500` | 19141
`poster/poster-tables.numbers/Data/droppedImage-small-18.png` | `2014-01-08 21:21:44.000000000 -0500` | 28425
`poster/poster-tables.numbers/Data/droppedImage-small-20.png` | `2014-01-08 21:21:44.000000000 -0500` | 11245
`poster/poster-tables.numbers/Index.zip` | `2014-01-29 18:07:26.000000000 -0500` | 285924
`poster/poster-tables.numbers/Metadata/BuildVersionHistory.plist` | `2014-01-29 13:44:09.000000000 -0500` | 595
`poster/poster-tables.numbers/Metadata/DocumentIdentifier` | `2014-01-08 21:23:21.000000000 -0500` | 36
`poster/poster-tables.numbers/Metadata/Properties.plist` | `2014-01-29 18:07:26.000000000 -0500` | 270
`poster/poster-tables.numbers/preview-micro.jpg` | `2014-01-29 13:46:15.000000000 -0500` | 1526
`poster/poster-tables.numbers/preview-web.jpg` | `2014-01-29 13:46:15.000000000 -0500` | 12106
`poster/poster-tables.numbers/preview.jpg` | `2014-01-29 13:46:15.000000000 -0500` | 127775
`poster/poster.graffle` | `2014-07-13 10:43:16.000000000 -0400` | 1815455
`poster/poster.pdf` | `2013-10-27 22:42:50.000000000 -0400` | 803970
`poster/uncovering-wtccc2-roc.pdf` | `2013-10-18 17:35:52.000000000 -0400` | 37286
