abbreviation	edges
G-a-D	[('gene', 'disease', 'association', 'both')]
G-a-D-s-D	[('gene', 'disease', 'association', 'both'), ('disease', 'disease', 'similarity', 'both')]
G-f-G-a-D	[('gene', 'gene', 'function', 'both'), ('gene', 'disease', 'association', 'both')]
G-e-T-c-D	[('gene', 'tissue', 'expression', 'both'), ('tissue', 'disease', 'cooccurrence', 'both')]
G-i-G-a-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'disease', 'association', 'both')]
