name	algorithm	arguments
PC	PC	{}
PCs	PCs	{}
PCt	PCt	{}
NPC	NPC	{}
DWPC_0.0	DWPC	{'damping_exponent': 0.0}
DWPC_0.1	DWPC	{'damping_exponent': 0.1}
DWPC_0.2	DWPC	{'damping_exponent': 0.2}
DWPC_0.3	DWPC	{'damping_exponent': 0.3}
DWPC_0.4	DWPC	{'damping_exponent': 0.4}
DWPC_0.5	DWPC	{'damping_exponent': 0.5}
DWPC_0.6	DWPC	{'damping_exponent': 0.6}
DWPC_0.7	DWPC	{'damping_exponent': 0.7}
DWPC_0.8	DWPC	{'damping_exponent': 0.8}
DWPC_0.9	DWPC	{'damping_exponent': 0.9}
DWPC_1.0	DWPC	{'damping_exponent': 1.0}
