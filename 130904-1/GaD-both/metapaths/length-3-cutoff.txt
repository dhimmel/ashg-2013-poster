abbreviation	edges
G-a-D	[('gene', 'disease', 'association', 'both')]
G-f-G-a-D	[('gene', 'gene', 'function', 'both'), ('gene', 'disease', 'association', 'both')]
G-s-T-p-D	[('gene', 'tissue', 'specificity', 'both'), ('tissue', 'disease', 'pathology', 'both')]
G-i-G-a-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'disease', 'association', 'both')]
G-a-D-a-G-a-D	[('gene', 'disease', 'association', 'both'), ('disease', 'gene', 'association', 'both'), ('gene', 'disease', 'association', 'both')]
G-a-D-p-T-p-D	[('gene', 'disease', 'association', 'both'), ('disease', 'tissue', 'pathology', 'both'), ('tissue', 'disease', 'pathology', 'both')]
G-a-D-i-F-i-D	[('gene', 'disease', 'association', 'both'), ('disease', 'factor', 'involvement', 'both'), ('factor', 'disease', 'involvement', 'both')]
G-f-G-f-G-a-D	[('gene', 'gene', 'function', 'both'), ('gene', 'gene', 'function', 'both'), ('gene', 'disease', 'association', 'both')]
G-f-G-s-T-p-D	[('gene', 'gene', 'function', 'both'), ('gene', 'tissue', 'specificity', 'both'), ('tissue', 'disease', 'pathology', 'both')]
G-f-G-i-G-a-D	[('gene', 'gene', 'function', 'both'), ('gene', 'gene', 'interaction', 'both'), ('gene', 'disease', 'association', 'both')]
G-s-T-s-G-a-D	[('gene', 'tissue', 'specificity', 'both'), ('tissue', 'gene', 'specificity', 'both'), ('gene', 'disease', 'association', 'both')]
G-i-G-f-G-a-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'gene', 'function', 'both'), ('gene', 'disease', 'association', 'both')]
G-i-G-s-T-p-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'tissue', 'specificity', 'both'), ('tissue', 'disease', 'pathology', 'both')]
G-i-G-i-G-a-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'gene', 'interaction', 'both'), ('gene', 'disease', 'association', 'both')]
