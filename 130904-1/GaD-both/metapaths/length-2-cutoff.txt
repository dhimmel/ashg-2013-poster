abbreviation	edges
G-a-D	[('gene', 'disease', 'association', 'both')]
G-f-G-a-D	[('gene', 'gene', 'function', 'both'), ('gene', 'disease', 'association', 'both')]
G-s-T-p-D	[('gene', 'tissue', 'specificity', 'both'), ('tissue', 'disease', 'pathology', 'both')]
G-i-G-a-D	[('gene', 'gene', 'interaction', 'both'), ('gene', 'disease', 'association', 'both')]
